# Spring Boot Security for Vaadin

This add-on serves as glue between Vaadin and Spring Boot Security with the goal of bringing both worlds together as seamlessly as possible. The URL-based security of Spring is replaced with annotations directly on your Vaadin views and endpoint methods. Access rules can be defined using expressions, like `hasRole('ADMIN')` or any Java code.

Version 4.0+ requires Vaadin 24 (and therefore also Java 17 and Spring Boot 3).  
Version 3.0+ requires Vaadin 23 (and Java 11).  
[Version 2.3+](https://gitlab.com/codecamp-de/vaadin-security-spring/-/tree/release-2.x) requires Vaadin 22.

Its features mainly focus on two areas:
* **Authentication:** You configure most parts of your Spring Security filter chain (AuthenticationManager, AuthenticationProviders, UserDetailsService, remember-be authentication, etc.) in your WebSecurityConfigurerAdapter as you would otherwise. This add-on will (by default) configure the form login and logout and wrap them in a simple Java (i.e. server-side) API. This API can be used in your login view to which the user will be automatically forwarded. This allows you to stay completely in Java and Vaadin; no HTML login page or URL redirection necessary. But authentication can also be completely customized to use Web SSO mechanisms instead. See the project page for links to examples like Keycloak and Kerberos.
* **Access control:** Access control works for views and [endpoints](https://vaadin.com/docs/current/flow/typescript/accessing-backend.html) using annotations. Access rules are defined using [Spring Security expressions](https://docs.spring.io/spring-security/site/docs/current/reference/html5/#el-access) or custom Java code for more advanced requirements. There's also a Java API to facilitate fine-grained control within your views and endpoints, so it is e.g. possible to also have publicly accessible views with partially restricted content. Access rules for routes can also be changed at runtime.

All code snippets here can also be found as part of a simple [demo application](https://gitlab.com/codecamp-de/vaadin-security-spring-demo).  
There's also variations of the demo for:
* [OAuth2 / OpenID Connect](https://gitlab.com/codecamp-de/vaadin-security-spring-demo/-/tree/oauth2)
* [Keycloak](https://gitlab.com/codecamp-de/vaadin-security-spring-demo/-/tree/keycloak)
* [Kerberos / SPNEGO](https://gitlab.com/codecamp-de/vaadin-security-spring-demo/-/tree/kerberos)

**Please be sure to pick a demo matching the version of this add-on you're using by selecting an appropriate branch.**

This add-on is published on [Maven Central](https://search.maven.org/artifact/de.codecamp.vaadin/vaadin-security-spring-boot-starter) and in the [Vaadin Add-on Directory](https://vaadin.com/directory/component/spring-boot-security-for-vaadin).

```xml
<dependency>
  <groupId>de.codecamp.vaadin</groupId>
  <artifactId>vaadin-security-spring-boot-starter</artifactId>
  <version>4.0.3</version>
</dependency>
```



## Customize the Spring Web Security Configuration

Like Spring Security, this add-on should just work enough out-of-the-box to get you started. Once you need to further customize the configuration and provide your own `SecurityFilterChain` for your Vaadin UI, you can use the `VaadinSecurityConfigurer` like in the following example.

```java
@Configuration
@EnableWebSecurity // this annotation might not be needed, depending on your Spring Boot version
public class WebSecurityConfig
{

  @Bean
  SecurityFilterChain vaadinSecurityFilterChain(HttpSecurity http)
    throws Exception
  {
    http.apply(new VaadinSecurityConfigurer());
    
    ...

    return http.build();
  }

}
```

The `VaadinSecurityConfigurer` does the following:
* Unless you have disabled standard authentication, it configures `formlogin()` and `logout()`, so you should refrain from doing that yourself, unless you really know what you're doing.
* It selectively disables CSRF protection, because Vaadin already takes care of that itself.
* It configures requestMatchers() so out of the box the Spring Security filter chain is only active for every request related to Vaadin. This is enough to secure the Vaadin UI, but you can add more if you wish to protect additional resources.

Other than that, configure the filter chain as you would otherwise (remember-me support etc.).

It's always strongly recommended to give your Vaadin UI its own base URL like `/ui/...`. This can avoid many potential issues and conflicts down the line. E.g. without it [certain static resources like WebJars will broken](https://github.com/vaadin/spring/issues/602).

```properties
# application.properties

# it's important to use urlMapping and NOT url-mapping
vaadin.urlMapping = /ui/*
```



## Authentication

The standard authentication provided by this add-on avoids any HTTP redirects and allows you to use a regular Vaadin Flow view as login page. But authentication can also be completely customized to e.g. use Web SSO mechanisms instead. See the linked demos at the top for examples on how to do that.


### Standard Authentication

The standard authentication provided by this add-on assumes and provides the following:

* Your login page is a regular Vaadin Flow view at the route `login`. You can change the path by setting the property `codecamp.vaadin.security.standard-auth.login-route`. See the demo project for a simple login view to get started.
* The `VaadinAuthenticationService` is used to perform the logins. This service is basically a convenient wrapper for Spring Security's form-based authentication endpoint.
* Accessing a protected route while not yet fully authenticated will forward the user to the login view.
* After successful authentication the user will be forwarded back to their original target view or the main view (root by default) as a fallback. `codecamp.vaadin.security.standard-auth.main-route` can be set to change the main view.

To override or extend the behavior of what happens after a successful or failed login attempt (using standard authentication), additional `AuthenticationResultHandler`s can be provided. They will be called in order and once `true` is returned, no further handlers are consulted. The handlers are ordered using the standard Spring machanism of `@Order` and/or `Ordered`. The default built-in handler mentioned above has the lowest precedence. Handlers registered like this are always considered after the handler passed to `VaadinAuthenticationService#login(...)`.

```java
@Component
public class CustomAuthenticationResultHandler
  implements AuthenticationResultHandler
{

  @Override
  public boolean handleAuthenticationResult(AuthenticationResult result)
  {
    // Return true if you want no further handling.

    return false;
  }

}
```



## Access Control

Access control for views and endpoints works in pretty much the same way: use the `@SecuredAccess` annotation.

* For views, annotate the navigation target and/or any parent layouts.
* For endpoints, annotate the endpoint method or the whole endpoint class. When no `@SecuredAccess` annotation is found, there's a fallback to [Vaadin's default security for endpoints](https://vaadin.com/docs/current/flow/typescript/configuring-security.html).

The `@SecuredAccess` annotation can either be used directly or as a meta-annotation which allows you to create reusable access rules. See the examples below.

Without any further attributes specified, this annotation simply requires a user to be authenticated.

### Expression-Based

One option to specify more specific access rules is to use [SpEL-based expressions](https://docs.spring.io/spring-security/site/docs/current/reference/html5/#el-access).

```java
@Route("admin")
@SecuredAccess("hasRole('ADMIN')")
public class AdminView
{
  ...
```

As previously mentioned, you can also make this access rule reusable like this:
```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SecuredAccess("hasRole('ADMIN')")
public @interface SecuredAdminOnly
{
}
```
```java
@Route("admin")
@SecuredAdminOnly
public class AdminView
{
  ...
```

### Custom Code

If you need more sophisticated logic to determine access you can instead implement a `AccessEvaluator`. 
This either needs to be Spring bean or a class with a public no-args constructor. You can then use
the evaluator in the `@AccessRoute` annotation.

```java
public class CustomAccessEvaluator
  implements AccessEvaluator
{

  @Override
  public boolean hasAccess(AccessContext accessContext)
  {
    ...
  }

}
```
```java
@Route("some")
@SecuredAccess(evaluator = CustomAccessEvaluator.class)
public class SomeView
{
  ...
```

### Predefined Annotations

This add-on provides some predefined annotations ready for use:

* `@PermitAll`: Allows access to everyone, authenticated or not. This is in line with Spring Security's meaning of 'permitAll', but different from `jakarta.annotation.security.PermitAll`.
* `@RequiresAuthentication`: Requires the user to be authenticated.
* `@RequiresFullAuthentication`: Requires the user to be fully authenticated. I.e. a remember-me authentication is not enough.
* `@RequiresRole`: Requires the user to have any of the specified roles. In terms of Spring Security, any authority prefixed with `ROLE_` is considered a role, the role name being everything after this prefix. So e.g. `@RequiresRole("admin")` will grant access to users with a granted authority of `ROLE_admin`.

### Setting Route Access Rules Dynamically at Runtime

Similarly to registering routes dynamically at runtime using Vaadin's [`RouteConfiguration`](https://vaadin.com/docs/flow/routing/tutorial-router-dynamic-routes.html) you can also dynamically set or override access rules for routes using `RouteAccessConfiguration`. Both the global application scope (`forApplicationScope()`) and the user-specific session scope (`forSessionScope()`) are available. There is currently nothing equivalent for endpoints.

As one would expect, you can set access rules for navigation targets or parent layouts:
```java
RouteAccessConfiguration.forApplicationScope().setAccessRule(DynamicRegistrationView.class, AccessRule.of("hasRole('ADMIN')"));
```
This will override any `@SecuredAccess` annotations that may be present on the respective classes.

Alternatively, you can also set access rules for route paths, regardless of what component will be shown there as navigation target.
```java
RouteAccessConfiguration.forApplicationScope().setAccessRule("dynamic/view", AccessRule.of("hasRole('ADMIN')"));
```
This will override any `@SecuredAccess` annotation that may be present on the navigation target and _even take precedence over dynamically set access rules for the navigation target_.

### Handle Route Access Denied

When access to a route has been denied, the following happens:

* The registered `RouteAccessDeniedHandler`s will be consulted - in order - until the first one triggers a forward or reroute. The handlers are ordered using the standard Spring machanism of `@Order` and/or `Ordered`, with the default order being `0`.
* If no handler triggers a forward or reroute, then a `RouteAccessDeniedException` is thrown. This can be handled by implementing a `HasErrorParameter` view for it, [Vaadin's router exception handling](https://vaadin.com/docs/current/flow/routing/tutorial-routing-exception-handling.html).

The `RouteAccessDeniedHandler` provided as part of the standard authentication implements the following behavior:
* When access is denied and the user is not fully authenticated (not authenticated at all or logged in via remember-me), the UI will be forwarded to the login view. Otherwise the default of throwing a `RouteAccessDeniedException` will apply.

### Inline Access Control

If you need to determine access *within* a view or endpoint (e.g. should certain UI elements or information be visible or not), you can use the methods of `VaadinSecurity`. This will work in all request threads and all threads accessing the UI or Vaadin session.

```java
if (VaadinSecurity.check().hasRole("ADMIN"))
{
  ...
}
```

Similarly, it's also possible to check access to a navigation target without navigation. E.g. to show certain links to views only when the user could actually go there.

```java
if (VaadinSecurity.hasAccessTo(AdminView.class))
{
  ...
}
```

This can of course be combined with route- or endpoint-based access control. E.g. to create a view that is only accessible by authenticated users, but contains additional information for managers.
