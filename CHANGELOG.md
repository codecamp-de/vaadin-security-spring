## Changelog

### 4.0.3

- Explicitly use AntPathRequestMatcher to avoid Spring's confusion when there are multiple servlets.


### 4.0.2

- Replace missed use of javax with jakarta.


### 4.0.1

- Prevent NPE when WrappedSession is missing.


### 4.0.0

- Support and require Vaadin 24.
- Support and require Spring Boot 3.
- Deprecate `RedirectView`. Can be replaced with `BeforeEvent#forwardToUrl(...)`.


### 3.1.2

- Make SecurityContext retrieval via VaadinSession available as static method.


### 3.1.1

- Restore old behavior of VaadinSecurityConfigurerAdapter.
- Workaround for missing authentication during AuthenticationChangeEvent.


### 3.1.0

- Migrate to `SecurityFilterChain` to prepare for removal of `WebSecurityConfigurerAdapter`.
- Set `SecurityContextHolderStrategy` to make `VaadinSecurity#getAuthentication()` obsolete.
- Fix availability for anonymous Authentications.


### 3.0.2

- Store original target also in session as fallback.


### 3.0.1

- Make endpoint security optional if dependency is missing.


### 3.0.0

- Support (and require) Vaadin 23.
- Build with Java 11.
- Update Spring Boot to 2.6.4.


### 2.3.0

- Support Vaadin 22. Will not work with previous versions!


### 2.2.1

- Fix error when no Fusion @Endpoint is present in the application.


### 2.2.0

- Support Vaadin 21. Will not work with previous versions!


### 2.1.1

 - Either expression or evaluator is now mandatory.


### 2.1.0

- Introduce AuthenticationChangeEvent.


### 2.0.2

- Don't fail in applications without endpoints.


### 2.0.1

- Improve error handling with security expression evaluation.


### 2.0.0

- Update to Vaadin 20, Spring Boot 2.4. Older versions no longer supported.
- Only selectively disable CSRF protection by default. Other non-Vaadin resources may still use it.
- Use Fetch API instead of 3rd-party lib.
- Add possibility to determine access to views without navigation.


### 1.0.0

- Add support for Vaadin endpoints. Routes and endpoints can now be secured in excatly the same way, while still allowing access to the specifics of each case when necessary. The necessary rework makes this version backwards incompatible, though the changes are not major. `@SecuredRoute` is now `@SecuredAccess`; the rest should be easier to figure out.


### 0.9.5

- Fix URL issues for standard authentication when using reverse proxies.
- Handlers have a better default order. Default handlers now provide constants with their order. Use them instead of magic numbers like `Ordered.LOWEST_PRECEDENCE - 1`.
- Other internal improvements.


### 0.9.4

- Authentication has been reworked to generally support Web SSO scenarios. See demo applications linked on the main project page.


### 0.9.3

- Authentication result handling is now customizable.
- Access rules can now be set dynamically (along the lines of `RouteConfiguration`).
- **A few classes have been renamed to be more fitting or concise.** This might cause a few minor compile errors that should be very easy to fix.


### 0.9.2

- Should work with Vaadin 17 now.
- Some fixes and improvements under the hood.


### 0.9.1

- This addon no longer triggers Spring Boot's ValidationAutoConfiguration. You no longer have to add *spring-boot-starter-validation* if you don't already need it for other reasons.


### 0.9.0

- Initial version
