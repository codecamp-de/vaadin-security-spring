package de.codecamp.vaadin.security.spring.authentication;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;


/**
 * {@link AuthenticationFailureHandler} that provides an appropriate {@link AuthenticationResult}.
 */
public class VaadinAuthenticationFailureHandler
  extends
    AbstractAuthenticationSuccessFailureHandler
  implements
    AuthenticationFailureHandler
{

  @Override
  public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
      AuthenticationException exception)
    throws IOException,
      ServletException
  {
    getSession(request).ifPresent(session ->
    {
      AuthenticationResult result = AuthenticationResult.failure(exception);
      session.accessSynchronously(() ->
      {
        session.setAttribute(AuthenticationResult.class, result);
      });
    });
  }

}
