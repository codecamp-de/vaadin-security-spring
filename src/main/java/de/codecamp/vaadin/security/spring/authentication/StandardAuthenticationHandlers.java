package de.codecamp.vaadin.security.spring.authentication;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.server.VaadinSession;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessDeniedHandler;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;


/**
 * Provides parts of the default behavior for standard authentication.
 */
@Order(StandardAuthenticationHandlers.ORDER)
public class StandardAuthenticationHandlers
  implements
    AuthenticationResultHandler,
    RouteAccessDeniedHandler
{

  /**
   * The {@link Ordered order} used for this handler.
   */
  public static final int ORDER = Ordered.LOWEST_PRECEDENCE;


  private static final Logger LOG = LoggerFactory.getLogger(StandardAuthenticationHandlers.class);

  /**
   * The key used to store the original navigation target when the user needs to be forwarded to the
   * login view.
   */
  private static final String DATA_ORIGINAL_TARGET = "VaadinAuthenticationOriginalTarget";


  private String mainRoute;

  private String loginRoute;


  public void setMainRoute(String mainRoute)
  {
    this.mainRoute = mainRoute;
  }

  public void setLoginRoute(String loginRoute)
  {
    this.loginRoute = loginRoute;
  }


  @Override
  public boolean handleAuthenticationResult(AuthenticationResult result)
  {
    if (result.isSuccess())
    {
      String originalTarget = recallOriginalTarget(UI.getCurrent());

      boolean onLoginView =
          UI.getCurrent().getInternals().getActiveViewLocation().getPath().equals(loginRoute);

      /*
       * Only navigate away when currently on the login view in order to not interfere with e.g.
       * inline login dialogs, like a login popup on an otherwise publicly accessible view.
       */
      if (onLoginView)
      {
        if (originalTarget == null || originalTarget.isBlank())
        {
          LOG.debug("Navigating from login route to main route '{}'.", mainRoute);

          UI.getCurrent().navigate(mainRoute);
        }
        else
        {
          LOG.debug("Navigating from login route to original target route '{}'.", originalTarget);

          UI.getCurrent().navigate(originalTarget);
        }
      }
      return true;
    }

    return false;
  }

  @Override
  public void handleAccessDenied(BeforeEnterEvent event)
  {
    /*
     * If the user is not yet fully authenticated (not logged in at all or logged in using the
     * remember me function), preserve the original target and redirect to the login view.
     */
    if (!VaadinSecurity.check().isFullyAuthenticated())
    {
      String originalTarget = event.getLocation().getPathWithQueryParameters();
      if (originalTarget.equals("."))
        originalTarget = "";

      storeOriginalTarget(event.getUI(), originalTarget);

      /*
       * forwardTo(...) requires a route path, i.e. without a leading slash.
       */
      LOG.debug("Forwarding to login route '{}'.", loginRoute);
      event.forwardTo(loginRoute);
    }
  }


  /**
   * Store the given original target in the given {@link UI} and the {@link UI#getSession()
   * VaadinSession} it belongs to.
   * <p>
   * Some authentication mechanisms might require the browser to request a certain URL, which will
   * destroy the current UI and everything stored in there. So as a fallback, the original target is
   * also stored in the {@link VaadinSession}.
   *
   * @param ui
   *          the UI
   * @param originalTarget
   *          the original target
   */
  public static void storeOriginalTarget(UI ui, String originalTarget)
  {
    requireNonNull(ui, "ui must not be null");

    ComponentUtil.setData(ui, DATA_ORIGINAL_TARGET, originalTarget);
    ui.getSession().setAttribute(DATA_ORIGINAL_TARGET, originalTarget);
  }

  /**
   * Recalls (gets and removes) the original target from the given UI or the {@link VaadinSession}
   * as a fallback.
   *
   * @param ui
   *          the UI
   * @return originalTarget the original target or null
   */
  public static String recallOriginalTarget(UI ui)
  {
    requireNonNull(ui, "ui must not be null");

    VaadinSession session = ui.getSession();

    String originalTarget = (String) ComponentUtil.getData(ui, DATA_ORIGINAL_TARGET);
    if (originalTarget != null)
    {
      ComponentUtil.setData(ui, DATA_ORIGINAL_TARGET, null);

      // only remove the original target from the session if it is the same
      if (Objects.equals(session.getAttribute(DATA_ORIGINAL_TARGET), originalTarget))
        session.setAttribute(DATA_ORIGINAL_TARGET, null);
    }

    // as a fallback also look in the Vaadin session
    if (originalTarget == null)
    {
      originalTarget = (String) session.getAttribute(DATA_ORIGINAL_TARGET);
      if (originalTarget != null)
        session.setAttribute(DATA_ORIGINAL_TARGET, null);
    }

    return originalTarget;
  }

}
