package de.codecamp.vaadin.security.spring.access;


import com.vaadin.flow.server.VaadinServletRequest;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.util.matcher.IpAddressMatcher;


/**
 * Extended expression root object for Vaadin.
 */
public class VaadinSecurityExpressionRoot
  extends
    SecurityExpressionRoot
  implements
    VaadinSecurityExpressionOperations
{

  private final VaadinServletRequest request;


  public VaadinSecurityExpressionRoot(Authentication authentication, VaadinServletRequest request)
  {
    super(authentication);
    this.request = request;
  }


  @Override
  public boolean hasIpAddress(String ipAddress)
  {
    return (new IpAddressMatcher(ipAddress).matches(request));
  }

}
