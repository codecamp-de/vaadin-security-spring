package de.codecamp.vaadin.security.spring.access;


import com.vaadin.flow.server.VaadinServletRequest;
import org.springframework.security.access.expression.AbstractSecurityExpressionHandler;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;


/**
 * The standard implementation of {@code VaadinSecurityExpressionHandler}.
 */
public class DefaultVaadinSecurityExpressionHandler
  extends
    AbstractSecurityExpressionHandler<VaadinServletRequest>
  implements
    VaadinSecurityExpressionHandler
{

  private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();

  private String defaultRolePrefix = "ROLE_";


  @Override
  public VaadinSecurityExpressionRoot createSecurityExpressionRoot(Authentication authentication,
      VaadinServletRequest request)
  {
    VaadinSecurityExpressionRoot root = new VaadinSecurityExpressionRoot(authentication, request);
    root.setPermissionEvaluator(getPermissionEvaluator());
    root.setTrustResolver(trustResolver);
    root.setRoleHierarchy(getRoleHierarchy());
    root.setDefaultRolePrefix(defaultRolePrefix);
    return root;
  }


  public void setTrustResolver(AuthenticationTrustResolver trustResolver)
  {
    this.trustResolver = trustResolver;
  }

  public void setDefaultRolePrefix(String defaultRolePrefix)
  {
    this.defaultRolePrefix = defaultRolePrefix;
  }

}
