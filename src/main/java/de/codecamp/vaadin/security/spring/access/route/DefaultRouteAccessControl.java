package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.AccessDeniedException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.HasErrorParameter;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouteNotFoundError;
import com.vaadin.flow.router.RouteParameters;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.router.internal.NavigationRouteTarget;
import com.vaadin.flow.router.internal.RouteTarget;
import com.vaadin.flow.server.RouteRegistry;
import com.vaadin.flow.server.SessionRouteRegistry;
import com.vaadin.flow.server.VaadinSession;
import de.codecamp.vaadin.security.spring.access.AccessEvaluator;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DefaultRouteAccessControl
  implements
    RouteAccessControl
{

  private static final Logger LOG = LoggerFactory.getLogger(DefaultRouteAccessControl.class);


  private boolean denyUnsecured;

  private List<RouteAccessDeniedHandler> accessDeniedHandlers;


  public void setDenyUnsecured(boolean denyUnsecured)
  {
    this.denyUnsecured = denyUnsecured;
  }

  public void setAccessDeniedHandlers(List<RouteAccessDeniedHandler> accessDeniedHandlers)
  {
    this.accessDeniedHandlers = accessDeniedHandlers;
  }


  @Override
  public boolean hasAccessTo(Class<? extends Component> navigationTarget)
  {
    RouteRegistry sessionRegistry =
        SessionRouteRegistry.getSessionRegistry(VaadinSession.getCurrent());

    RouteTarget routeTarget =
        sessionRegistry.getRouteTarget(navigationTarget, RouteParameters.empty());
    if (routeTarget == null)
    {
      LOG.warn("Could not find registered route for navigation target '{}'.", navigationTarget);
      return false;
    }

    String routePath =
        RouteConfiguration.forRegistry(sessionRegistry).getUrlBase(navigationTarget).orElse(null);
    if (routePath == null)
    {
      LOG.warn("Could not find registered route for navigation target '{}'.", navigationTarget);
      return false;
    }

    return hasAccess(null, routeTarget.getTarget(), routeTarget.getParentLayouts(), null);
  }

  @Override
  public boolean hasAccessTo(String routePath)
  {
    NavigationRouteTarget navigationRouteTarget = SessionRouteRegistry
        .getSessionRegistry(VaadinSession.getCurrent()).getNavigationRouteTarget(routePath);
    if (!navigationRouteTarget.hasTarget())
    {
      LOG.warn("Could not find navigation target for route path '{}'.", routePath);
      return false;
    }

    RouteTarget routeTarget = navigationRouteTarget.getRouteTarget();
    return hasAccess(routePath, routeTarget.getTarget(), routeTarget.getParentLayouts(), null);
  }

  @Override
  public void checkAccess(BeforeEnterEvent event)
  {
    LOG.trace("Checking access to route '{}' ({}).", event.getLocation().getPath(),
        event.getNavigationTarget().getName());

    // grant access to HasErrorParameters
    // grant access to RouteNotFoundError only when already authenticated
    if (HasErrorParameter.class.isAssignableFrom(event.getNavigationTarget())
        || (RouteNotFoundError.class.isAssignableFrom(event.getNavigationTarget())
            && VaadinSecurity.check().isAuthenticated()))
    {
      LOG.debug("Access granted to error view '{}' at '{}'.", event.getNavigationTarget().getName(),
          event.getLocation().getPath());
      return;
    }

    /*
     * Don't check access when navigating from Flow views to Fusion views (represented by the
     * navigation target
     * com.vaadin.flow.component.internal.JavaScriptBootstrapUI$ClientViewPlaceholder).
     */
    if (event.getNavigationTarget().getName().contains("ClientViewPlaceholder"))
    {
      LOG.debug("Ignoring navigation to client-side view at '{}'.", event.getLocation().getPath());
      return;
    }



    @SuppressWarnings("unchecked")
    boolean hasAccess = hasAccess(event.getLocation().getPath(),
        (Class<? extends Component>) event.getNavigationTarget(), event.getLayouts(),
        event.getUI().getSession());

    if (hasAccess)
    {
      LOG.debug("Access granted to route '{}' ({}).", event.getLocation().getPath(),
          event.getNavigationTarget().getName());
      return;
    }


    LOG.debug("Access denied to route '{}' ({}).", event.getLocation().getPath(),
        event.getNavigationTarget().getName());
    onAccessDenied(event);
  }

  protected boolean hasAccess(String routePath, Class<? extends Component> navigationTarget,
      List<Class<? extends RouterLayout>> parentLayouts, VaadinSession vaadinSession)
  {
    if (vaadinSession == null)
      vaadinSession = VaadinSession.getCurrent();
    if (vaadinSession == null)
      throw new IllegalStateException("No VaadinSession available.");

    boolean secured = false;
    boolean hasAccess = true;

    List<Class<?>> targetAndLayouts = new ArrayList<>();
    targetAndLayouts.add(navigationTarget);
    targetAndLayouts.addAll(parentLayouts);

    SessionRouteAccessRuleRegistry accessRuleRegistry =
        SessionRouteAccessRuleRegistry.getSessionRegistry(vaadinSession);

    for (Class<?> targetOrLayout : targetAndLayouts)
    {
      AccessRule accessRule = null;

      // access rules defined for the path will take precedence over rules for the navigation target
      if (targetOrLayout == navigationTarget)
        accessRule = accessRuleRegistry.getAccessRule(routePath).orElse(null);

      if (accessRule == null)
        accessRule = accessRuleRegistry.getAccessRule(targetOrLayout).orElse(null);

      if (accessRule == null)
        continue;


      secured = true;

      if (accessRule.expression() != null)
      {
        if (!VaadinSecurity.hasAccess(accessRule.expression()))
        {
          hasAccess = false;
          break;
        }
      }
      if (accessRule.evaluator() != null)
      {
        AccessEvaluator evaluator =
            vaadinSession.getService().getInstantiator().getOrCreate(accessRule.evaluator());
        if (!evaluator.hasAccess(new RouteAccessContext(targetOrLayout)))
        {
          hasAccess = false;
          break;
        }
      }
    }

    if (denyUnsecured && !secured)
    {
      hasAccess = false;
    }

    return hasAccess;
  }

  protected void onAccessDenied(BeforeEnterEvent event)
  {
    if (event.hasForwardTarget() || event.hasRerouteTarget() || event.hasErrorParameter())
    {
      // a AccessEvaluator seems to have set a forward or reroute target that will be respected
      return;
    }

    for (RouteAccessDeniedHandler handler : accessDeniedHandlers)
    {
      handler.handleAccessDenied(event);
      if (event.hasForwardTarget() || event.hasRerouteTarget() || event.hasErrorParameter())
        return;
    }

    if (!event.hasForwardTarget() && !event.hasRerouteTarget() && !event.hasErrorParameter())
    {
      throw new AccessDeniedException();
    }
  }

}
