package de.codecamp.vaadin.security.spring.access;


import com.vaadin.flow.server.VaadinServletRequest;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.core.Authentication;


/**
 * Security expression handler specifically for Vaadin.
 */
public interface VaadinSecurityExpressionHandler
  extends
    SecurityExpressionHandler<VaadinServletRequest>
{

  VaadinSecurityExpressionRoot createSecurityExpressionRoot(Authentication authentication,
      VaadinServletRequest request);

}
