package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.ListenerPriority;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.server.VaadinServiceInitListener;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessConfiguration;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StandardAuthenticationServiceInitListener
  implements
    VaadinServiceInitListener,
    UIInitListener
{

  private static final Logger LOG =
      LoggerFactory.getLogger(StandardAuthenticationServiceInitListener.class);

  private final LoginRouteAccessListener loginRouteAccessListener = new LoginRouteAccessListener();


  private String loginRoute;

  private String mainRoute;


  public void setLoginRoute(String loginRoute)
  {
    this.loginRoute = loginRoute;
  }

  public void setMainRoute(String mainRoute)
  {
    this.mainRoute = mainRoute;
  }


  @Override
  public void serviceInit(ServiceInitEvent serviceInitEvent)
  {
    // always grant access to login route
    LOG.debug("Setting access rule for login route '{}'.", loginRoute);
    RouteAccessConfiguration.forApplicationScope().setAccessRule(loginRoute,
        AccessRule.of("permitAll"));
  }

  @Override
  public void uiInit(UIInitEvent event)
  {
    event.getUI().addBeforeEnterListener(loginRouteAccessListener);
  }


  @ListenerPriority(LoginRouteAccessListener.PRIORITY) // execute after RouteAccessControl
  private class LoginRouteAccessListener
    implements
      BeforeEnterListener
  {

    private static final int PRIORITY = RouteAccessControl.PRIORITY - 10;


    @Override
    public void beforeEnter(BeforeEnterEvent event)
    {
      // forward to main route or original target if already fully authenticated
      if (event.getLocation().getPath().equals(loginRoute)
          && VaadinSecurity.check().isFullyAuthenticated())
      {
        String originalTarget = StandardAuthenticationHandlers.recallOriginalTarget(event.getUI());
        if (originalTarget == null || originalTarget.isBlank())
        {
          LOG.debug("Already fully authenticated. Forwarding from login route to main route '{}'.",
              mainRoute);

          event.forwardTo(mainRoute);
        }
        else
        {
          LOG.debug("Already fully authenticated. Forwarding from login route to original target"
              + " route '{}'.", originalTarget);

          event.forwardTo(originalTarget);
        }
      }
    }

  }

}
