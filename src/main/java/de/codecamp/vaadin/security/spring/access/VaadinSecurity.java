package de.codecamp.vaadin.security.spring.access;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinSession;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessControl;
import jakarta.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.Optional;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;


/**
 * Utility class to programmatically evaluate Spring Security expressions (see
 * {@link #hasAccess(String)}) or call operations available to those expressions directly (see
 * {@link #check()}).
 * <p>
 * The {@link Authentication} is available in all request threads (Vaadin request or otherwise) and
 * in all threads where the {@link VaadinSession#getCurrent() current Vaadin session} is available.
 *
 * @see <a href=
 *      "http://docs.spring.io/spring-security/site/docs/current/reference/html/el-access.html">Expression-Based
 *      Access Control</a>
 */
public final class VaadinSecurity
{

  private static final SpelExpressionParser SPEL_PARSER = new SpelExpressionParser();


  private VaadinSecurity()
  {
    // utility class
  }


  /**
   * Returns {@link VaadinSecurityExpressionOperations operations} that allow to check for certain
   * access of the current {@link Authentication}. The available operations are the same as in the
   * expressions.
   * <p>
   * The {@link Authentication} is available in all request threads (Vaadin request or otherwise)
   * and in all threads where the {@link VaadinSession#getCurrent() current Vaadin session} is
   * available.
   *
   * @return the security operations
   */
  public static VaadinSecurityExpressionOperations check()
  {
    return check(getRequiredAuthentication());
  }

  /**
   * Check for access by querying the methods of the returned
   * {@link VaadinSecurityExpressionOperations}. The available operations are the same as in the
   * expressions.
   *
   * @param authentication
   *          the authentication to be tested
   * @return the security operations
   */
  public static VaadinSecurityExpressionOperations check(Authentication authentication)
  {
    Objects.requireNonNull(authentication, "authentication must not be null");

    return getExpressionHandler().createSecurityExpressionRoot(authentication,
        VaadinServletRequest.getCurrent());
  }

  /**
   * Returns whether the current {@link Authentication} has access based on the given security
   * expression.
   * <p>
   * The {@link Authentication} is available in all request threads (Vaadin request or otherwise)
   * and in all threads where the {@link VaadinSession#getCurrent() current Vaadin session} is
   * available.
   *
   * @param securityExpression
   *          the Spring Security expression
   * @return whether the current security context has access
   * @see SecurityContextHolder#getContext()
   */
  public static boolean hasAccess(String securityExpression)
  {
    return hasAccess(getRequiredAuthentication(), securityExpression);
  }

  /**
   * Returns whether the given {@link Authentication} has access based on the specified Spring
   * security expression.
   *
   * @param authentication
   *          the authentication to be tested
   * @param securityExpression
   *          the Spring Security expression
   * @return whether the current security context has access
   */
  public static boolean hasAccess(Authentication authentication, String securityExpression)
  {
    Objects.requireNonNull(authentication, "authentication must not be null");

    EvaluationContext evaluationContext = getExpressionHandler()
        .createEvaluationContext(authentication, VaadinServletRequest.getCurrent());
    boolean hasAccess;
    try
    {
      hasAccess = SPEL_PARSER.parseExpression(securityExpression).getValue(evaluationContext,
          Boolean.class);
    }
    catch (IllegalStateException | ParseException | EvaluationException ex)
    {
      throw new AccessRuleException("Failed to evaluate security expression: " + securityExpression,
          ex);
    }
    return hasAccess;
  }

  private static VaadinSecurityExpressionHandler getExpressionHandler()
  {
    return getBean(VaadinSecurityExpressionHandler.class);
  }

  private static Optional<HttpServletRequest> getCurrentHttpRequest()
  {
    return Optional.ofNullable(RequestContextHolder.getRequestAttributes())
        .filter(ServletRequestAttributes.class::isInstance) //
        .map(ServletRequestAttributes.class::cast) //
        .map(ServletRequestAttributes::getRequest);
  }

  private static <BEAN> BEAN getBean(Class<BEAN> beanType)
  {
    // Vaadin request thread or UI thread
    UI ui = UI.getCurrent();
    if (ui != null)
      return Instantiator.get(ui).getOrCreate(beanType);

    // Vaadin Connect endpoint thread or any plain non-Vaadin request thread
    WebApplicationContext applicationContext = getApplicationContext().orElse(null);
    if (applicationContext != null)
      return applicationContext.getBean(beanType);

    throw new IllegalStateException("No current UI or HTTP request available.");
  }

  private static Optional<WebApplicationContext> getApplicationContext()
  {
    return getCurrentHttpRequest().map(request -> WebApplicationContextUtils
        .getWebApplicationContext(request.getServletContext()));
  }

  /**
   * Returns whether the current {@link Authentication} has access to the given navigation target.
   *
   * @param navigationTarget
   *          the navigation targets to be checked for access
   * @return whether the current security context has access
   * @see SecurityContextHolder#getContext()
   */
  public static boolean hasAccessTo(Class<? extends Component> navigationTarget)
  {
    return getBean(RouteAccessControl.class).hasAccessTo(navigationTarget);
  }

  /**
   * Returns whether the current {@link Authentication} has access to the given route path.
   *
   * @param routePath
   *          the route path to be checked for access
   * @return whether the current security context has access
   * @see SecurityContextHolder#getContext()
   */
  public static boolean hasAccessTo(String routePath)
  {
    return getBean(RouteAccessControl.class).hasAccessTo(routePath);
  }

  /**
   * Returns the current {@link Authentication} or throws an exception otherwise. The
   * {@link Authentication} is available in all request threads (Vaadin request or otherwise) and in
   * all threads where the {@link VaadinSession#getCurrent() current Vaadin session} is available.
   *
   * @return the current {@link Authentication}
   * @throws IllegalStateException
   *           if no authentication is found
   */
  private static Authentication getRequiredAuthentication()
  {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null)
    {
      throw new IllegalStateException("No Authentication found. Authentication is available in all"
          + " request threads and wherever VaadinSession#getCurrent() is available.");
    }
    return authentication;
  }

  /**
   * Returns the current {@link Authentication}, if available. The {@link Authentication} is
   * available in all request threads (Vaadin request or otherwise) and in all threads where the
   * {@link VaadinSession#getCurrent() current Vaadin session} is available.
   *
   * @return the current {@link Authentication} if available, null otherwise
   * @deprecated just use {@code SecurityContextHolder.getContext().getAuthentication()}
   */
  @Deprecated
  public static Authentication getAuthentication()
  {
    return SecurityContextHolder.getContext().getAuthentication();
  }

}
