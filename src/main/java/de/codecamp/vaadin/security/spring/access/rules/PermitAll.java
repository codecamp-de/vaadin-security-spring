package de.codecamp.vaadin.security.spring.access.rules;


import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Allows access to everyone, authenticated or not. I.e. this is Spring Security's definition of
 * {@code permitAll}. This is in contrast to {@link jakarta.annotation.security.PermitAll} which
 * only grants access to every <em>authenticated</em> user.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@SecuredAccess("permitAll")
public @interface PermitAll
{
}
