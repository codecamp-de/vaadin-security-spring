package de.codecamp.vaadin.security.spring.access.rules;


import de.codecamp.vaadin.security.spring.access.AccessContext;
import de.codecamp.vaadin.security.spring.access.AccessEvaluator;
import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Optional;


@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@SecuredAccess(evaluator = RequiresRole.Evaluator.class)
public @interface RequiresRole
{

  String[] value();


  class Evaluator
    implements
      AccessEvaluator
  {

    @Override
    public boolean hasAccess(AccessContext accessContext)
    {
      Optional<RequiresRole> requiresRoleOpt = accessContext.findAnnotation(RequiresRole.class);

      if (!requiresRoleOpt.isPresent())
        return false;

      return VaadinSecurity.check().hasAnyRole(requiresRoleOpt.get().value());
    }

  }

}
