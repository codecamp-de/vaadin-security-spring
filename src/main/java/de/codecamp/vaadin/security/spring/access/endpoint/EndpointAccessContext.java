package de.codecamp.vaadin.security.spring.access.endpoint;


import de.codecamp.vaadin.security.spring.access.AccessContext;
import jakarta.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Optional;
import org.springframework.core.annotation.AnnotationUtils;


/**
 * An {@link AccessContext} encapsulating an access attempt to an endpoint.
 */
public class EndpointAccessContext
  implements
    AccessContext
{

  private final Method endpointMethod;

  private final HttpServletRequest request;


  /**
   * Creates a new {@link EndpointAccessContext}.
   *
   * @param endpointMethod
   *          the endpoint method to be checked for access
   * @param request
   *          the HTTP request trying to access the endpoint
   */
  public EndpointAccessContext(Method endpointMethod, HttpServletRequest request)
  {
    this.endpointMethod = endpointMethod;
    this.request = request;
  }


  @Override
  public TargetType getTargetType()
  {
    return TargetType.ENDPOINT;
  }

  public Method getEndpointMethod()
  {
    return endpointMethod;
  }

  public HttpServletRequest getRequest()
  {
    return request;
  }

  @Override
  public <A extends Annotation> Optional<A> findAnnotation(Class<A> annotationType)
  {
    return findAnnotation(endpointMethod, annotationType);
  }

  /**
   * Tries to find the first matching annotation of the given type on the endpoint method. If none
   * is found, the endpoint class is examined. This method also examines superclasses and interfaces
   * and supports meta-annotations.
   *
   * @param <A>
   *          the annotation type
   * @param endpointMethod
   *          the endpoint method
   * @param annotationType
   *          the annotation type being looked for
   * @return the first matching annotation
   */
  public static <A extends Annotation> Optional<A> findAnnotation(Method endpointMethod,
      Class<A> annotationType)
  {
    A annotation = AnnotationUtils.findAnnotation(endpointMethod, annotationType);
    if (annotation == null)
    {
      annotation =
          AnnotationUtils.findAnnotation(endpointMethod.getDeclaringClass(), annotationType);
    }
    return Optional.ofNullable(annotation);
  }

}
