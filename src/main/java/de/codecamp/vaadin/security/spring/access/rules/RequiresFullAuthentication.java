package de.codecamp.vaadin.security.spring.access.rules;


import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Requires the user to be fully authenticated. The equivalent Spring Security expression is
 * {@code isFullyAuthenticated()}.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@SecuredAccess("isFullyAuthenticated()")
public @interface RequiresFullAuthentication
{
}
