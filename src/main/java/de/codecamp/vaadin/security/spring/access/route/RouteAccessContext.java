package de.codecamp.vaadin.security.spring.access.route;


import de.codecamp.vaadin.security.spring.access.AccessContext;
import java.lang.annotation.Annotation;
import java.util.Optional;
import org.springframework.core.annotation.AnnotationUtils;


/**
 * An {@link AccessContext} encapsulating an access attempt to a route.
 */
public class RouteAccessContext
  implements
    AccessContext
{

  private final Class<?> accessedTargetOrLayout;


  /**
   * Creates a new {@link RouteAccessContext}.
   *
   * @param targetOrLayout
   *          the target or parent layout class currently being checked for access
   */
  public RouteAccessContext(Class<?> targetOrLayout)
  {
    this.accessedTargetOrLayout = targetOrLayout;
  }


  @Override
  public TargetType getTargetType()
  {
    return TargetType.ROUTE;
  }

  /**
   * Returns the navigation target itself or one of its parent layouts currently being checked for
   * access.
   *
   * @return the target or parent layout class currently being checked for access
   */
  public Class<?> getAccessedTargetOrLayout()
  {
    return accessedTargetOrLayout;
  }

  @Override
  public <A extends Annotation> Optional<A> findAnnotation(Class<A> annotationType)
  {
    return findAnnotation(accessedTargetOrLayout, annotationType);
  }

  /**
   * Tries to find the first matching annotation of the given type on the navigation target or
   * layout class. This method also examines superclasses and interfaces and supports
   * meta-annotations.
   *
   * @param <A>
   *          the annotation type
   * @param targetOrLayout
   *          the navigation target or layout class
   * @param annotationType
   *          the annotation type being looked for
   * @return the first matching annotation
   */
  public <A extends Annotation> Optional<A> findAnnotation(Class<?> targetOrLayout,
      Class<A> annotationType)
  {
    return Optional.ofNullable(AnnotationUtils.findAnnotation(targetOrLayout, annotationType));
  }

}
