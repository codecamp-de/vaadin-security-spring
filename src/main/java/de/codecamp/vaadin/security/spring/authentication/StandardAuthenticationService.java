package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.dom.DomListenerRegistration;
import com.vaadin.flow.server.VaadinSession;
import elemental.json.Json;
import elemental.json.JsonObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class StandardAuthenticationService
  implements
    VaadinAuthenticationService
{

  private static final Logger LOG = LoggerFactory.getLogger(StandardAuthenticationService.class);


  /**
   * URL for the login processing endpoint suitable for the client, i.e. relative to the base URI of
   * the page (which is the root of the Vaadin UI)
   */
  private String loginProcessingClientUrl;

  /**
   * URL for the logout endpoint suitable for the client, i.e. relative to the base URI of the page
   * (which is the root of the Vaadin UI)
   */
  private String logoutProcessingClientUrl;

  private List<AuthenticationResultHandler> authenticationResultHandlers;


  public StandardAuthenticationService()
  {
  }


  public void setLoginProcessingClientUrl(String loginProcessingClientUrl)
  {
    this.loginProcessingClientUrl = loginProcessingClientUrl;
  }

  public void setLogoutProcessingClientUrl(String logoutProcessingClientUrl)
  {
    this.logoutProcessingClientUrl = logoutProcessingClientUrl;
  }

  public void setAuthenticationResultHandlers(
      List<AuthenticationResultHandler> authenticationResultHandlers)
  {
    this.authenticationResultHandlers = authenticationResultHandlers;
  }


  @Override
  public void login(Component commComponent, String username, String password, boolean rememberMe,
      AuthenticationResultHandler primaryHandler)
  {
    Objects.requireNonNull(commComponent, "commComponent must not be null");
    Objects.requireNonNull(username, "username must not be null");
    Objects.requireNonNull(password, "password must not be null");

    if (!commComponent.getElement().isEnabled())
    {
      throw new IllegalStateException(
          "The communication component must not be disabled for the login to work.");
    }


    JsonObject loginData = Json.createObject();
    // these are the default parameter names for form logins
    loginData.put("username", username);
    loginData.put("password", password);
    loginData.put("remember-me", rememberMe);

    AtomicReference<DomListenerRegistration> listenerRegistration = new AtomicReference<>();
    listenerRegistration.set(commComponent.getElement().addEventListener("login-done", event ->
    {
      try
      {
        AuthenticationResult result;
        try
        {
          result = retrieveLoginResult();
        }
        catch (RuntimeException ex)
        {
          LOG.error("An error occurred while trying to retrieve the login result.", ex);
          result = AuthenticationResult.failure(ex);
        }

        handleResult(result, primaryHandler);
      }
      finally
      {
        Optional.ofNullable(listenerRegistration.get()).ifPresent(DomListenerRegistration::remove);
      }
    }));

    // POST to the URL provided by Spring Security for form-based authentication
    String loginScript = """
        let element = this;
        const loginData = %s;
        const urlData = new URLSearchParams();

        for (var key in loginData) {
          urlData.set(key, loginData[key]);
        }

        fetch('%s', {
          method: 'POST',
          body: urlData
        })
        .finally(() => {
          element.dispatchEvent(new Event('login-done'));
        });""" //
        .formatted(loginData.toJson(), loginProcessingClientUrl);
    commComponent.getElement().executeJs(loginScript);
  }

  private AuthenticationResult retrieveLoginResult()
  {
    VaadinSession session = VaadinSession.getCurrent();
    AuthenticationResult result = session.getAttribute(AuthenticationResult.class);
    if (result == null)
    {
      throw new IllegalStateException(
          "VaadinAuthenticationResult not available. It seems VaadinAuthenticationSuccessHandler"
              + " and/or VaadinAuthenticationFailureHandler are not set up correctly.");
    }

    session.setAttribute(AuthenticationResult.class, null);

    return result;
  }

  private void handleResult(AuthenticationResult result, AuthenticationResultHandler primaryHandler)
  {
    List<AuthenticationResultHandler> handlers = new ArrayList<>();
    if (primaryHandler != null)
      handlers.add(primaryHandler);
    if (authenticationResultHandlers != null)
      handlers.addAll(authenticationResultHandlers);

    for (AuthenticationResultHandler h : handlers)
    {
      if (h.handleAuthenticationResult(result))
        break;
    }
  }


  @Override
  public void logout(Component commComponent)
  {
    Objects.requireNonNull(commComponent, "commComponent must not be null");

    // POST to the URL provided by Spring Security for logout
    String logoutScript = """
        fetch('%s', {
          method: 'POST'
        })
        .then(response => {
          window.location.href = response.url;
        });""" //
        .formatted(logoutProcessingClientUrl);
    commComponent.getElement().executeJs(logoutScript);
  }

}
