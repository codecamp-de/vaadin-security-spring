package de.codecamp.vaadin.security.spring.access;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * This annotation is used to secure routes and endpoints. It can also be used as meta-annotation to
 * create reusable access rule annotations.
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SecuredAccess
{

  /**
   * A Spring Security expression to control access.
   * <p>
   * If both an expression and an evaluator are set, both must be satisfied to grant access.
   *
   * @return a Spring Security expression
   * @see <a href=
   *      "https://docs.spring.io/spring-security/site/docs/current/reference/htmlsingle/#el-access">Expression-Based
   *      Access Control</a>
   */
  String value() default EXPRESSION_NOT_SET;

  /**
   * A {@link AccessEvaluator programmatic evaluator} to control access.
   * <p>
   * If both an expression and an evaluator are set, both must be satisfied to grant access.
   *
   * @return a {@link AccessEvaluator}
   */
  Class<? extends AccessEvaluator> evaluator() default NotSetAccessEvaluator.class;


  /**
   * The default string used when no expression has been set.
   */
  String EXPRESSION_NOT_SET = "___NOT_SET___";


  /**
   * The default class used when no evaluator has been set.
   */
  class NotSetAccessEvaluator
    implements
      AccessEvaluator
  {

    @Override
    public boolean hasAccess(AccessContext accessContext)
    {
      throw new UnsupportedOperationException("NotSetAccessEvaluator cannot be evaluated.");
    }

  }

}
