package de.codecamp.vaadin.security.spring.access.endpoint;


import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import de.codecamp.vaadin.security.spring.access.AccessEvaluator;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import de.codecamp.vaadin.security.spring.access.VaadinSecurity;
import jakarta.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.security.Principal;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;


public class EndpointAccessChecker
  extends
    dev.hilla.auth.EndpointAccessChecker
{

  private static final Logger LOG = LoggerFactory.getLogger(EndpointAccessChecker.class);


  private final ConcurrentMap<Method, AccessRule> accessRuleCache = new ConcurrentHashMap<>();


  public EndpointAccessChecker(AccessAnnotationChecker accessAnnotationChecker)
  {
    super(accessAnnotationChecker);
  }


  @Override
  public String check(Method method, HttpServletRequest request)
  {
    return doCheck(method, request);
  }

  @Override
  public String check(Method method, Principal principal, Function<String, Boolean> rolesChecker)
  {
    /*
     * check(Method, HttpServletRequest) is apparently not used/called anymore. But this variant
     * provides less information and therefore flexibility. So reacquire the request from the
     * RequestContextHolder.
     */
    HttpServletRequest request = Optional.ofNullable(RequestContextHolder.getRequestAttributes())
        .filter(ServletRequestAttributes.class::isInstance)
        .map(ServletRequestAttributes.class::cast).map(ServletRequestAttributes::getRequest)
        .orElse(null);
    if (request == null)
    {
      LOG.error("The current HttpServletRequest could not be acquired when checking Vaadin endpoint"
          + " access.");
      return ACCESS_DENIED_MSG;
    }

    return doCheck(method, request);
  }

  private String doCheck(Method method, HttpServletRequest request)
  {
    LOG.trace("Checking access to endpoint method '{} # {}(...)' in {}.",
        method.getDeclaringClass().getSimpleName(), method.getName(),
        method.getDeclaringClass().getName());

    AccessRule accessRule = accessRuleCache.computeIfAbsent(method, m ->
    {
      return EndpointAccessContext.findAnnotation(method, SecuredAccess.class)
          .map(AccessRule::asCopyOf).orElse(null);
    });


    if (accessRule == null)
    {
      // no @SecuredAccess found, use Vaadin's original behavior
      LOG.debug(
          "Delegating to Vaadin's default access control for endpoint method '{} # {}(...)' in {}.",
          method.getDeclaringClass().getSimpleName(), method.getName(),
          method.getDeclaringClass().getName());
      return super.check(method, request.getUserPrincipal(), request::isUserInRole);
    }


    boolean hasAccess = true;

    if (accessRule.expression() != null)
    {
      String expression = accessRule.expression();
      if (!VaadinSecurity.hasAccess(expression))
      {
        hasAccess = false;
      }
    }
    if (hasAccess && accessRule.evaluator() != null)
    {
      WebApplicationContext applicationContext =
          WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
      AccessEvaluator evaluator = applicationContext.getBean(accessRule.evaluator());
      if (!evaluator.hasAccess(new EndpointAccessContext(method, request)))
      {
        hasAccess = false;
      }
    }

    if (hasAccess)
    {
      LOG.debug("Access granted to endpoint method '{} # {}(...)' in {}.",
          method.getDeclaringClass().getSimpleName(), method.getName(),
          method.getDeclaringClass().getName());
      return null;
    }
    else
    {
      LOG.debug("Access denied to endpoint method '{} # {}(...)' in {}.",
          method.getDeclaringClass().getSimpleName(), method.getName(),
          method.getDeclaringClass().getName());
      return ACCESS_DENIED_MSG;
    }
  }

}
