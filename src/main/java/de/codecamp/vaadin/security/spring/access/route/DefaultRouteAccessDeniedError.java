package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.AccessDeniedException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ErrorParameter;
import com.vaadin.flow.router.HasErrorParameter;
import jakarta.servlet.http.HttpServletResponse;


public class DefaultRouteAccessDeniedError
  extends
    Composite<VerticalLayout>
  implements
    HasErrorParameter<AccessDeniedException>
{

  @Override
  public int setErrorParameter(BeforeEnterEvent event,
      ErrorParameter<AccessDeniedException> parameter)
  {
    getContent().add("You don't have the required permissions to access this view.");
    return HttpServletResponse.SC_FORBIDDEN;
  }

}
