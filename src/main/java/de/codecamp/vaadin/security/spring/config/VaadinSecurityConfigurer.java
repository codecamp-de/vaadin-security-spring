package de.codecamp.vaadin.security.spring.config;


import static org.apache.commons.lang3.StringUtils.defaultIfBlank;

import com.vaadin.flow.spring.security.RequestUtil;
import de.codecamp.vaadin.security.spring.access.endpoint.EndpointAccessChecker;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessControl;
import de.codecamp.vaadin.security.spring.authentication.VaadinAuthenticationFailureHandler;
import de.codecamp.vaadin.security.spring.authentication.VaadinAuthenticationService;
import de.codecamp.vaadin.security.spring.authentication.VaadinAuthenticationSuccessHandler;
import de.codecamp.vaadin.security.spring.autoconfigure.VaadinSecurityProperties;
import de.codecamp.vaadin.security.spring.autoconfigure.VaadinSecurityProperties.StandardAuth;
import jakarta.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;


/**
 * Configures a Spring Security filter chain for Vaadin.
 * <ul>
 * <li>Disables CSRF support for Vaadin-related requests. Vaadin already handles this on its
 * own.</li>
 * <li>Only specific URLs
 * ({@link de.codecamp.vaadin.security.spring.autoconfigure.VaadinSecurityProperties#getUiRootUrl()
 * UI root URL} and {@code /VAADIN}) and requests
 * ({@link RequestUtil#isFrameworkInternalRequest(HttpServletRequest) framework} and
 * {@link RequestUtil#isEndpointRequest(HttpServletRequest) endpoint} requests) will pass through
 * the Spring Security filter chain.</li>
 * <li>Does NOT {@link HttpSecurity#authorizeRequests() restrict access} based on URLs or requests.
 * Access control is handled by {@link RouteAccessControl} on a per view-basis and by
 * {@link EndpointAccessChecker} per endpoint class or method.</li>
 * <li>If enabled, configures the form login and logout endpoints as needed for the standard
 * authentication provided by the {@link VaadinAuthenticationService}.</li>
 * </ul>
 * Apply this configuration to a {@link HttpSecurity} like this:
 * <p>
 * {@code http.apply(new VaadinSecurityConfigurer());}
 */
public class VaadinSecurityConfigurer
  extends
    AbstractHttpConfigurer<VaadinSecurityConfigurer, HttpSecurity>
{

  private boolean configured = false;


  @Override
  public void init(HttpSecurity http)
    throws Exception
  {
    doConfigure(http);
  }

  @Override
  public void configure(HttpSecurity http)
    throws Exception
  {
    doConfigure(http);
  }

  /**
   * {@link VaadinSecurityConfigurer} is a proper {@link AbstractHttpConfigurer} now. As such, when
   * {@link HttpSecurity#apply(org.springframework.security.config.annotation.SecurityConfigurerAdapter)
   * applied to a HttpSecurity}, other configurers need to be called in {@link #init(HttpSecurity)}
   * now. To not unnecessarily break things, the old way of calling {@link #configure(HttpSecurity)}
   * directly still works for now, by ensuring that the actual configuration is only performed once.
   */
  private void doConfigure(HttpSecurity http)
    throws Exception // NOPMD:SignatureDeclareThrowsException
  {
    if (configured)
      return;


    ApplicationContext applicationContext = http.getSharedObject(ApplicationContext.class);
    VaadinSecurityProperties properties =
        applicationContext.getBean(VaadinSecurityProperties.class);
    RequestUtil requestUtil = applicationContext.getBean(RequestUtil.class);


    /*
     * Configure which requests will pass through the filter chain of this security configuration.
     * I.e. only during those requests the SecurityContext will be available. The goal is to be as
     * selective as possible (to not interfere with other security configurations) while still
     * allowing this add-on to do its job.
     */
    List<String> rootPatterns = new ArrayList<>();
    if (properties.getUiRootUrl().isEmpty())
    {
      rootPatterns.add("/**");
    }
    else
    {
      rootPatterns.add(properties.getUiRootUrl());
      rootPatterns.add(properties.getUiRootUrl() + "/**");
    }
    RequestMatcher[] rootPatternRequestMatchers =
        rootPatterns.stream().map(AntPathRequestMatcher::antMatcher).toArray(RequestMatcher[]::new);

    http.securityMatchers(conf -> conf //
        .requestMatchers(requestUtil::isFrameworkInternalRequest) //
        .requestMatchers(requestUtil::isEndpointRequest) //
        .requestMatchers(rootPatternRequestMatchers));


    // CSRF is already handled by Vaadin.
    http.csrf(conf -> conf //
        .ignoringRequestMatchers(requestUtil::isFrameworkInternalRequest) //
        .ignoringRequestMatchers(requestUtil::isEndpointRequest) //
        .ignoringRequestMatchers(rootPatternRequestMatchers));


    /*
     * The standard authentication makes use of the form-login functionality and replaces the
     * handlers with versions that allow Vaadin navigation instead of HTTP-based redirects.
     */
    StandardAuth standardAuthProperties = properties.getStandardAuth();
    if (standardAuthProperties.isEnabled())
    {
      http.formLogin(conf -> conf //
          .loginProcessingUrl(standardAuthProperties.getLoginProcessingUrl()) //
          .successHandler(new VaadinAuthenticationSuccessHandler()) //
          .failureHandler(new VaadinAuthenticationFailureHandler()));

      http.logout(conf -> conf //
          .logoutUrl(standardAuthProperties.getLogoutProcessingUrl()) //
          .logoutSuccessUrl(defaultIfBlank(standardAuthProperties.getLogoutSuccessUrl(),
              defaultIfBlank(properties.getUiRootUrl(), "/"))));
    }

    configured = true;
  }

}
