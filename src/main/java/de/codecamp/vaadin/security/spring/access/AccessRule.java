package de.codecamp.vaadin.security.spring.access;


import de.codecamp.vaadin.security.spring.access.SecuredAccess.NotSetAccessEvaluator;
import java.io.Serializable;
import java.util.Objects;


/**
 * Represents the rule determining access. If both an expression and an {@link AccessEvaluator} are
 * specified, both must be satisfied to gain access. At least one of them must be specified.
 */
public final class AccessRule
  implements
    Serializable
{

  private final String expression;

  private final Class<? extends AccessEvaluator> evaluator;


  private AccessRule(SecuredAccess securedAccess)
  {
    this.expression =
        !securedAccess.value().equals(SecuredAccess.EXPRESSION_NOT_SET) ? securedAccess.value()
            : null;
    this.evaluator =
        securedAccess.evaluator() != NotSetAccessEvaluator.class ? securedAccess.evaluator() : null;

    if (expression == null && evaluator == null)
    {
      throw new AccessRuleException(
          "Either a security expression or an AccessEvaluator must be specified.");
    }
  }

  private AccessRule(String expression)
  {
    Objects.requireNonNull(expression, "expression must not be null");

    this.expression = expression;
    this.evaluator = null;
  }

  private AccessRule(Class<? extends AccessEvaluator> evaluator)
  {
    Objects.requireNonNull(evaluator, "evaluator must not be null");

    this.expression = null;
    this.evaluator = evaluator;
  }


  /**
   * A Spring Security expression to control access; may be null.
   * <p>
   * If both an expression and an evaluator are set, both must be satisfied to grant access.
   *
   * @return a Spring Security expression; may be null
   */
  public String expression()
  {
    return expression;
  }

  /**
   * A {@link AccessEvaluator programmatic evaluator} to control access; may be null.
   * <p>
   * If both an expression and an evaluator are set, both must be satisfied to grant access.
   *
   * @return a {@link AccessEvaluator}; may be null
   */
  public Class<? extends AccessEvaluator> evaluator()
  {
    return evaluator;
  }


  /**
   * Creates a new access rule as copy from a {@link SecuredAccess} annotation.
   *
   * @param securedAccess
   *          the {@link SecuredAccess} annotation
   * @return the access rule
   * @see SecuredAccess
   */
  public static AccessRule asCopyOf(SecuredAccess securedAccess)
  {
    return new AccessRule(securedAccess);
  }

  /**
   * Creates a new access rule based on an expression.
   *
   * @param expression
   *          a Spring Security expression
   * @return the access rule
   * @see SecuredAccess#value()
   */
  public static AccessRule of(String expression)
  {
    return new AccessRule(expression);
  }

  /**
   * Creates a new access rule based on an evaluator.
   *
   * @param evaluator
   *          a {@link AccessEvaluator}
   * @return the access rule
   * @see SecuredAccess#evaluator()
   */
  public static AccessRule of(Class<? extends AccessEvaluator> evaluator)
  {
    return new AccessRule(evaluator);
  }

}
