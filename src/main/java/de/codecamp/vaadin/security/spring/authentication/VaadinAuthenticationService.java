package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.login.LoginForm;


public interface VaadinAuthenticationService
{

  /**
   * Returns the {@link VaadinAuthenticationService}. This method must be called from a request
   * handling or UI thread.
   *
   * @return the {@link VaadinAuthenticationService}
   */
  static VaadinAuthenticationService get()
  {
    return get(UI.getCurrent());
  }

  /**
   * Returns the {@link VaadinAuthenticationService} associated with the UI of the given component.
   *
   * @param component
   *          any attached component from your UI or the UI itself
   * @return the {@link VaadinAuthenticationService}
   */
  static VaadinAuthenticationService get(Component component)
  {
    try
    {
      return component.getUI().get().getSession().getService().getInstantiator()
          .getOrCreate(VaadinAuthenticationService.class);
    }
    catch (RuntimeException ex)
    {
      throw new IllegalStateException(
          "VaadinAuthenticationService not available. Please enable standard authentication.", ex);
    }
  }


  /**
   * Performs the authentication attempt against Spring Security and call the provided handler with
   * the result.
   * <p>
   * The provided {@code commComponent} used for communication with the client-side <b>must be
   * enabled</b>. So don't pass in a {@link LoginForm} or a {@link Button} with "disable on click".
   *
   * @param commComponent
   *          any component in the login view that will be used for communication with the
   *          client-side; <b>the component must be attached and enabled</b>
   * @param username
   *          the user name
   * @param password
   *          the password
   * @param rememberMe
   *          whether remember-me should be used; only has an effect when that feature is actually
   *          activated
   * @param primaryHandler
   *          the handler that will be notified first with the result once the login attempt has
   *          completed; may be null
   */
  void login(Component commComponent, String username, String password, boolean rememberMe,
      AuthenticationResultHandler primaryHandler);


  /**
   * Logs out the current user. This method must be called from a request handling or UI thread.
   *
   * @see #logout(Component)
   */
  default void logout()
  {
    logout(UI.getCurrent());
  }

  /**
   * Logs out the user associated with the UI of the given component. Affects all UIs in the user's
   * session, not just the provided one.
   *
   * @param commComponent
   *          any attached component from your UI or the UI itself
   */
  void logout(Component commComponent);

}
