package de.codecamp.vaadin.security.spring.authentication;


import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


/**
 * {@link AuthenticationSuccessHandler} that provides an appropriate {@link AuthenticationResult}.
 */
public class VaadinAuthenticationSuccessHandler
  extends
    AbstractAuthenticationSuccessFailureHandler
  implements
    AuthenticationSuccessHandler
{

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
      Authentication authentication)
    throws IOException,
      ServletException
  {
    getSession(request).ifPresent(session ->
    {
      AuthenticationResult result = AuthenticationResult.success();
      session.accessSynchronously(() ->
      {
        session.setAttribute(AuthenticationResult.class, result);
      });
    });
  }

}
