package de.codecamp.vaadin.security.spring.autoconfigure;


import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.stripEnd;

import com.vaadin.flow.spring.VaadinConfigurationProperties;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;


/**
 * Configuration properties for {@link VaadinSecurityAutoConfiguration}.
 */
@Validated
@ConfigurationProperties(prefix = VaadinSecurityProperties.PREFIX)
public class VaadinSecurityProperties
{

  public static final String PREFIX = "codecamp.vaadin.security";


  @Autowired
  private VaadinConfigurationProperties vaadinProperties;


  /**
   * The root URL of the Vaadin UI, relative to the servlet context. The default is determined based
   * on the <b>vaadin.urlMapping</b> property.
   */
  private String uiRootUrl;

  /**
   * Whether to deny access to any unsecured routes. When active, use e.g.
   * <b>{@link de.codecamp.vaadin.security.spring.access.rules.PermitAll @PermitAll}</b> to
   * explicitly allow access by everyone.
   */
  private boolean denyUnsecured = false;

  @Valid
  @NestedConfigurationProperty
  private final StandardAuth standardAuth = new StandardAuth();


  public String getUiRootUrl()
  {
    if (isBlank(uiRootUrl))
    {
      uiRootUrl = stripEnd(vaadinProperties.getUrlMapping(), "/*");
    }
    return uiRootUrl;
  }

  public boolean getDenyUnsecured()
  {
    return denyUnsecured;
  }

  public void setDenyUnsecured(boolean denyUnsecured)
  {
    this.denyUnsecured = denyUnsecured;
  }


  public StandardAuth getStandardAuth()
  {
    return standardAuth;
  }


  public class StandardAuth
  {

    /**
     * Whether the standard authentication provided by the VaadinAuthenticationService should be
     * enabled.
     */
    private boolean enabled = true;

    /**
     * The Vaadin route of the main view. Used only as fallback after a successful login when the
     * user can't be forwarded back to the view that triggered the authentication. Also used to
     * determine the default of the <b>logout-success-url</b> property.
     */
    private String mainRoute = "";


    /**
     * The Vaadin route of the login view.
     */
    private String loginRoute = "login";

    /**
     * The URL that processes the form-based authentication, relative to the servlet context. The
     * default is determined based on the <b>login-route</b> property. There's usually no need to
     * change this.
     */
    private String loginProcessingUrl;

    /**
     * The URL that triggers a log out, relative to the servlet context. The default is
     * <b>/logout</b> relative to the <b>ui-root-url</b> property. There's usually no need to change
     * this.
     */
    private String logoutProcessingUrl;

    /**
     * The URL the user is redirected to after logging out, relative to the servlet context. The
     * default is determined based on the <b>main-route</b> property.
     */
    private String logoutSuccessUrl;


    public boolean isEnabled()
    {
      return enabled;
    }

    public void setEnabled(boolean enabled)
    {
      this.enabled = enabled;
    }

    public String getMainRoute()
    {
      return mainRoute;
    }

    public void setMainRoute(String mainRoute)
    {
      this.mainRoute = mainRoute;
    }

    public String getLoginRoute()
    {
      return loginRoute;
    }

    public void setLoginRoute(String loginRoute)
    {
      this.loginRoute = loginRoute;
    }

    public String getLoginProcessingUrl()
    {
      if (isBlank(loginProcessingUrl))
      {
        loginProcessingUrl = getUiRootUrl() + "/" + getLoginRoute();
      }
      return loginProcessingUrl;
    }

    public String getLogoutProcessingUrl()
    {
      if (isBlank(logoutProcessingUrl))
      {
        logoutProcessingUrl = getUiRootUrl() + "/logout";
      }
      return logoutProcessingUrl;
    }

    public String getLogoutSuccessUrl()
    {
      if (isBlank(logoutSuccessUrl))
      {
        logoutSuccessUrl = getUiRootUrl() + "/" + getMainRoute();
      }
      return logoutSuccessUrl;
    }

    public void setLogoutSuccessUrl(String logoutSuccessUrl)
    {
      this.logoutSuccessUrl = logoutSuccessUrl;
    }

  }

}
