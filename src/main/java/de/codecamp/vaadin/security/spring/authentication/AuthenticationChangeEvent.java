package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.server.VaadinSession;
import java.util.EventObject;
import org.springframework.security.core.Authentication;


/**
 * Event object containing information about the changed authentication.
 */
public class AuthenticationChangeEvent
  extends
    EventObject
{

  private final Authentication authentication;


  public AuthenticationChangeEvent(VaadinSession session, Authentication authentication)
  {
    super(session);
    this.authentication = authentication;
  }


  /**
   * Returns the session for which the authentication changed.
   *
   * @return the session for which the authentication changed
   */
  public VaadinSession getSession()
  {
    return (VaadinSession) getSource();
  }

  /**
   * Returns the new authentication.
   *
   * @return the new authentication
   */
  public Authentication getAuthentication()
  {
    return authentication;
  }

}
