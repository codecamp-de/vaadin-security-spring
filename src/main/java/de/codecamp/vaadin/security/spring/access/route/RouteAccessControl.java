package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.ListenerPriority;


/**
 * Checks access before entering a navigation target and can evaluate access to specific navigation
 * targets or route paths.
 */
public interface RouteAccessControl
{

  /**
   * The {@link ListenerPriority priority} used to register the {@link BeforeEnterListener} used to
   * perform access control. The value is very high but still allows other listeners before it.
   */
  int PRIORITY = Integer.MAX_VALUE - 10;


  boolean hasAccessTo(Class<? extends Component> navigationTarget);

  boolean hasAccessTo(String routePath);

  void checkAccess(BeforeEnterEvent event);

}
