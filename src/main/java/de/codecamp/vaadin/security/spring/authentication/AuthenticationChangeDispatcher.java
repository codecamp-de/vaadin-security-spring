package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.dom.Element;
import com.vaadin.flow.router.EventUtil;
import com.vaadin.flow.server.VaadinSession;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


/**
 * Delivers an {@link AuthenticationChangeEvent} to all (attached) components that implement
 * {@link AuthenticationChangeObserver} and belong to the same session.
 */
public class AuthenticationChangeDispatcher
{

  private static final Logger LOG = LoggerFactory.getLogger(AuthenticationChangeDispatcher.class);


  @EventListener({AuthenticationSuccessEvent.class, InteractiveAuthenticationSuccessEvent.class})
  public void handleAuthenticationSuccess(AbstractAuthenticationEvent authenticationSuccessEvent)
  {
    ServletRequestAttributes requestAttributes =
        (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    if (requestAttributes == null)
    {
      LOG.warn("Failed to deliver AuthenticationChangeEvent. No current request context found.");
      return;
    }

    HttpServletRequest httpRequest = requestAttributes.getRequest();

    getVaadinSession(httpRequest).ifPresent(session ->
    {
      fireAuthenticationChangeEvent(session, authenticationSuccessEvent.getAuthentication());
    });
  }

  private Optional<VaadinSession> getVaadinSession(HttpServletRequest request)
  {
    HttpSession httpSession = request.getSession(false);
    if (httpSession == null)
      return Optional.empty();

    /*
     * There could only be more than one VaadinSession if there are multiple Vaadin servlets within
     * the same application. Not sure how to properly handle that so it's not yet supported for now.
     */
    Collection<VaadinSession> sessions = VaadinSession.getAllSessions(httpSession);
    if (sessions.size() > 1)
    {
      throw new IllegalStateException(
          "Multiple VaadinSessions (caused by multiple VaadinServlets) not supported.");
    }

    VaadinSession session = sessions.stream().findFirst().orElse(null);

    return Optional.ofNullable(session);
  }

  private void fireAuthenticationChangeEvent(VaadinSession session, Authentication authentication)
  {
    AuthenticationChangeEvent event = new AuthenticationChangeEvent(session, authentication);

    session.access(() ->
    {
      /*
       * In some situations the current SecurityContext still doesn't contain the new
       * Authentication. Whenever that's the case temporarily store it in a new context so any code
       * executed by the AuthenticationChangeObservers can access it in the usual way.
       *
       * It's still unclear what causes this, however.
       */
      SecurityContext origContext = SecurityContextHolder.getContext();
      if (origContext.getAuthentication() == null)
      {
        SecurityContext newContext = SecurityContextHolder.createEmptyContext();
        newContext.setAuthentication(authentication);
        SecurityContextHolder.setContext(newContext);
      }
      else
      {
        origContext = null;
      }

      session.getUIs().forEach(ui -> ui.accessSynchronously(() ->
      {
        Collection<Element> descendants = new ArrayList<>();
        EventUtil.inspectHierarchy(ui.getElement(), descendants, element -> true);
        EventUtil
            .getImplementingComponents(descendants.stream(), AuthenticationChangeObserver.class)
            .forEach(observer -> observer.authenticationChange(event));
      }));

      // restore original context if it was changed
      if (origContext != null)
        SecurityContextHolder.setContext(origContext);
    });
  }

}
