package de.codecamp.vaadin.security.spring.access;


public class AccessRuleException
  extends
    RuntimeException
{

  public AccessRuleException(String message)
  {
    super(message);
  }

  public AccessRuleException(String message, Throwable cause)
  {
    super(message, cause);
  }

}
