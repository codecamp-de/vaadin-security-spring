package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.server.VaadinContext;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServlet;
import com.vaadin.flow.server.VaadinSession;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;


/**
 * Allows to dynamically set the access rules for certain routes, navigation targets and parent
 * layouts. This will override rules defined on navigation targets and parent layouts directly via
 * {@link SecuredAccess}.
 * <p>
 * This is essentially the security counterpart for
 * {@link com.vaadin.flow.router.RouteConfiguration}. As such, there's an application and a session
 * scope, where the session scope can also see rules from the application scope. Both scopes
 * consider {@link SecuredAccess} annotations on navigation targets and parent layouts when they
 * have not been overriden.
 */
public final class RouteAccessConfiguration
  implements
    Serializable
{

  private final AbstractRouteAccessRuleRegistry registry;


  private RouteAccessConfiguration(AbstractRouteAccessRuleRegistry registry)
  {
    this.registry = registry;
  }


  /**
   * Returns a configurator for application scoped access rules. This requires that
   * {@link VaadinServlet#getCurrent()} is populated.
   *
   * @return configurator for application scoped access rules
   */
  public static RouteAccessConfiguration forApplicationScope()
  {
    VaadinContext context = VaadinService.getCurrent().getContext();
    if (context == null)
    {
      throw new IllegalStateException("VaadinContext not found. Application scope only available in"
          + " request or UI threads or in ServiceInitLister.");
    }
    return new RouteAccessConfiguration(
        ApplicationRouteAccessRuleRegistry.getApplicationRegistry(context));
  }

  /**
   * Returns a configurator for session scoped access rules. This requires that
   * {@link VaadinSession#getCurrent()} is populated.
   * <p>
   * <em>The session scope also returns access rules from the application scope.</em>
   *
   * @return configurator for session scoped routes
   */
  public static RouteAccessConfiguration forSessionScope()
  {
    VaadinSession session = VaadinSession.getCurrent();
    if (session == null)
    {
      throw new IllegalStateException(
          "VaadinSession not found. Session scope only available in" + " request or UI threads.");
    }

    return new RouteAccessConfiguration(SessionRouteAccessRuleRegistry.getSessionRegistry(session));
  }


  /**
   * Sets the access rule for the given navigation target or parent layout.
   * <p>
   * <em>Setting an access rule for a path will take precedence over access rules set for the target
   * class.</em>
   *
   * @param targetOrLayout
   *          the navigation target or parent layout
   * @param accessRule
   *          the access rule
   */
  public void setAccessRule(Class<? extends Component> targetOrLayout, AccessRule accessRule)
  {
    registry.setAccessRule(targetOrLayout, accessRule);
  }

  /**
   * Removes the access rule for the given navigation target or parent layout.
   *
   * @param targetOrLayout
   *          the navigation target or parent layout
   */
  public void removeAccessRule(Class<? extends Component> targetOrLayout)
  {
    registry.removeAccessRule(targetOrLayout);
  }

  /**
   * Returns the effective access rule for the given navigation target or parent layout.
   *
   * @param targetOrLayout
   *          the navigation target or parent layout
   * @return the effective access rule for the navigation target or parent layout
   */
  public Optional<AccessRule> getAccessRule(Class<?> targetOrLayout)
  {
    return registry.getAccessRule(targetOrLayout);
  }

  /**
   * Returns all access rules registered for navigation targets or parent layouts.
   *
   * @return all access rules registered for navigation targets or parent layouts
   */
  public Map<String, AccessRule> getRegisteredAccessRulesForTargetsAndLayouts()
  {
    return registry.getRegisteredAccessRulesForTargetsAndLayouts();
  }


  /**
   * Sets the access rule for the given route path.
   * <p>
   * <em>Setting an access rule for a path will take precedence over access rules set for the target
   * class. Access rules for parent layouts are still considered.</em>
   *
   * @param routePath
   *          the route path
   * @param accessRule
   *          the access rule
   */
  public void setAccessRule(String routePath, AccessRule accessRule)
  {
    registry.setAccessRule(routePath, accessRule);
  }

  /**
   * Removes the access rule for the given route path.
   *
   * @param routePath
   *          the route path
   */
  public void removeAccessRule(String routePath)
  {
    registry.removeAccessRule(routePath);
  }

  /**
   * Returns the effective access rule for the given route path.
   *
   * @param routePath
   *          the route path
   * @return the effective access rule for the given route path
   */
  public Optional<AccessRule> getAccessRule(String routePath)
  {
    return registry.getAccessRule(routePath);
  }

  /**
   * Returns all access rules registered route paths.
   *
   * @return all access rules registered route paths
   */
  public Map<String, AccessRule> getRegisteredAccessRulesForPaths()
  {
    return registry.getRegisteredAccessRulesForPaths();
  }

}
