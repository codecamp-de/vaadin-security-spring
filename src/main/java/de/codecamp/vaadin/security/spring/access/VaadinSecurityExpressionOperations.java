package de.codecamp.vaadin.security.spring.access;


import org.springframework.security.access.expression.SecurityExpressionOperations;


public interface VaadinSecurityExpressionOperations
  extends
    SecurityExpressionOperations
{

  boolean hasIpAddress(String ipAddress);

}
