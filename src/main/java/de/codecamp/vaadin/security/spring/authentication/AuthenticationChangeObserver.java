package de.codecamp.vaadin.security.spring.authentication;


import java.io.Serializable;


/**
 * Any {@code com.vaadin.ui.Component} implementing this interface will be informed when a user's
 * authentication changes after a successful login.
 */
@FunctionalInterface
public interface AuthenticationChangeObserver
  extends
    Serializable
{

  void authenticationChange(AuthenticationChangeEvent event);

}
