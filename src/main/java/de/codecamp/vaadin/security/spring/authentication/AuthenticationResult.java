package de.codecamp.vaadin.security.spring.authentication;


import java.io.Serializable;
import java.util.Optional;
import org.springframework.security.core.AuthenticationException;


/**
 * Contains the result of an authentication attempt.
 */
public final class AuthenticationResult
  implements
    Serializable
{

  private final boolean success;

  private final Exception exception;


  private AuthenticationResult(boolean success, Exception exception)
  {
    this.success = success;
    this.exception = exception;
  }


  public boolean isSuccess()
  {
    return success;
  }

  public boolean isFailure()
  {
    return !isSuccess();
  }

  /**
   * The exception associated with the authentication failure, if available. Typically this is a
   * subclass of {@link AuthenticationException}.
   *
   * @return exception associated with the authentication failure
   */
  public Optional<Exception> getException()
  {
    return Optional.ofNullable(exception);
  }


  public static AuthenticationResult success()
  {
    return new AuthenticationResult(true, null);
  }

  public static AuthenticationResult failure(Exception exception)
  {
    return new AuthenticationResult(false, exception);
  }

}
