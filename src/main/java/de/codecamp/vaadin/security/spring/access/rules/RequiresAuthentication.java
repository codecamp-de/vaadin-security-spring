package de.codecamp.vaadin.security.spring.access.rules;


import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Requires the user to be authenticated. The equivalent Spring Security expression is
 * {@code isAuthenticated()}.
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@SecuredAccess("isAuthenticated()")
public @interface RequiresAuthentication
{
}
