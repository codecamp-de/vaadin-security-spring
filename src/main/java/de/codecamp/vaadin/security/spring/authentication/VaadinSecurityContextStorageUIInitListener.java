package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.ListenerPriority;
import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.server.VaadinSession;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;


/**
 * Registers a {@link BeforeEnterListener} on every UI to always store the latest
 * {@link SecurityContext} with an anonymous authentication in the {@link VaadinSession} until the
 * user is authenticated. See {@link VaadinSecurityContextHolderStrategy} for why this is necessary.
 */
public class VaadinSecurityContextStorageUIInitListener
  implements
    UIInitListener
{

  private static final SecurityContextSessionBinder SECURITY_CONTEXT_SESSION_BINDER =
      new SecurityContextSessionBinder();


  @Override
  public void uiInit(UIInitEvent event)
  {
    // make SecurityContext available in all UI threads
    event.getUI().addBeforeEnterListener(SECURITY_CONTEXT_SESSION_BINDER);
  }


  @ListenerPriority(Integer.MAX_VALUE)
  private static class SecurityContextSessionBinder
    implements
      BeforeEnterListener
  {

    @Override
    public void beforeEnter(BeforeEnterEvent event)
    {
      SecurityContext securityContext = SecurityContextHolder.getContext();
      if (securityContext.getAuthentication() instanceof AnonymousAuthenticationToken)
      {
        event.getUI().getSession().setAttribute(SecurityContext.class, securityContext);
      }
      else
      {
        event.getUI().getSession().setAttribute(SecurityContext.class, null);
      }
    }

  }

}
