package de.codecamp.vaadin.security.spring.util;


import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Page;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;


/**
 * Vaadin does not offer a way to redirect to a(n external) URL (as opposed to another view).
 * {@link Page#setLocation(String)} would work, but does not prevent the original target view from
 * being shown first. So this class provides a workaround until
 * <a href="https://github.com/vaadin/flow/issues/4606">issue #4606</a> is implemented in Vaadin.
 * <p>
 * Simply call {@link #redirectToUrl(BeforeEnterEvent, String)} to redirect to a URL.
 *
 * @deprecated Use {@link BeforeEnterEvent#forwardToUrl(String)} instead.
 */
@Route(RedirectView.ROUTE_PATH)
@Deprecated(since = "4.0.0", forRemoval = true)
public class RedirectView
  extends
    Composite<VerticalLayout>
  implements
    BeforeEnterObserver
{

  public static final String ROUTE_PATH = "redirect";

  private static final String DATA_TARGET = "RedirectTarget";


  @Override
  public void beforeEnter(BeforeEnterEvent event)
  {
    String targetUrl = (String) ComponentUtil.getData(event.getUI(), DATA_TARGET);

    getContent().removeAll();
    Div div = new Div();
    getContent().add(div);

    if (targetUrl != null)
    {
      ComponentUtil.setData(event.getUI(), DATA_TARGET, null);

      div.add("Redirecting to ");
      div.add(new Anchor(targetUrl, targetUrl));

      event.getUI().getPage().setLocation(targetUrl);
    }
    else
    {
      div.add("Redirect view entered without target.");
    }
  }

  public static void redirectToUrl(BeforeEnterEvent event, String targetUrl)
  {
    ComponentUtil.setData(event.getUI(), DATA_TARGET, targetUrl);

    /*
     * Even though a reroute does not change the URL, the target view needs a route anyway.
     * setRoute(...) will throw an exception if the route is already defined, so use the session
     * scope to avoid concurrency issues.
     */
    if (!RouteConfiguration.forSessionScope().isPathAvailable(ROUTE_PATH))
    {
      RouteConfiguration.forSessionScope().setRoute(ROUTE_PATH, RedirectView.class);
    }

    event.rerouteTo(RedirectView.class);
  }

}
