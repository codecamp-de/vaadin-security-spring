package de.codecamp.vaadin.security.spring.authentication;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.server.VaadinSession;
import java.util.Optional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.util.Assert;


/**
 * A {@link SecurityContextHolderStrategy} that also uses the {@link VaadinSession#getCurrent()
 * current VaadinSession}, if available, to determine the {@link SecurityContext}. Otherwise the
 * behavior is that of the default {@link SecurityContextHolder#MODE_THREADLOCAL}.
 * <p>
 * After authentication the {@link SecurityContext} is (unless configured otherwise) stored in the
 * HTTP session via a {@link HttpSessionSecurityContextRepository}, which can be accessed wherever
 * the {@link VaadinSession#getCurrent() current VaadinSession} is available. However, security
 * contexts with an anonymous authentication are not saved and instead recreated with every request
 * by the {@link AnonymousAuthenticationFilter}. This means that when e.g. accessing the Vaadin UI
 * from a background thread no authentication would be available; Spring Security however expects
 * any kind of {@link Authentication} when checking access rights. For that reason the
 * {@link VaadinSecurityContextStorageUIInitListener} always stores the latest anonymous
 * authentication in the Vaadin session.
 */
public class VaadinSecurityContextHolderStrategy
  implements
    SecurityContextHolderStrategy
{

  private static final ThreadLocal<SecurityContext> CONTEXT_HOLDER = new ThreadLocal<>();


  @Override
  public void clearContext()
  {
    CONTEXT_HOLDER.remove();
  }

  @Override
  public SecurityContext getContext()
  {
    SecurityContext context = Optional.ofNullable(CONTEXT_HOLDER.get())
        .or(this::getSecurityContextFromCurrentVaadinSession).orElse(null);
    if (context == null)
    {
      context = createEmptyContext();
      CONTEXT_HOLDER.set(context);
    }
    return context;
  }

  /**
   * Try to get the {@link SecurityContext} via the current {@link VaadinSession}.
   *
   * @return the security context
   */
  private Optional<SecurityContext> getSecurityContextFromCurrentVaadinSession()
  {
    VaadinSession vaadinSession = VaadinSession.getCurrent();
    if (vaadinSession == null || vaadinSession.getSession() == null)
      return Optional.empty();

    return getSecurityContextFromVaadinSession(vaadinSession);
  }

  /**
   * Get the {@link SecurityContext} via the given {@link VaadinSession}, if available.
   *
   * @param vaadinSession
   *          the Vaadin session
   * @return the security context
   */
  public static Optional<SecurityContext> getSecurityContextFromVaadinSession(
      VaadinSession vaadinSession)
  {
    requireNonNull(vaadinSession, "vaadinSession must not be null");

    /*
     * 1) Try to get the SecurityContext from the HTTP session where the
     * HttpSessionSecurityContextRepository would save it.
     *
     * 2) Try to get the SecurityContext from the VaadinSession where the
     * VaadinSecurityContextStorageServiceInitListener would save it.
     */
    return Optional.ofNullable(vaadinSession.getSession())
        .map(s -> s.getAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY))
        .filter(SecurityContext.class::isInstance).map(SecurityContext.class::cast)
        .or(() -> Optional.ofNullable(vaadinSession.getAttribute(SecurityContext.class)));
  }

  @Override
  public void setContext(SecurityContext context)
  {
    Assert.notNull(context, "Only non-null SecurityContext instances are permitted");
    CONTEXT_HOLDER.set(context);
  }

  @Override
  public SecurityContext createEmptyContext()
  {
    return new SecurityContextImpl();
  }

}
