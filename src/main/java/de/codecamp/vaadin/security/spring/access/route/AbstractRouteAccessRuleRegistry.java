package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.component.Component;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import de.codecamp.vaadin.security.spring.access.SecuredAccess;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.core.annotation.AnnotationUtils;


public abstract class AbstractRouteAccessRuleRegistry
  implements
    Serializable
{

  private final Map<Class<?>, AccessRule> rulesForTargetOrLayouts = new HashMap<>();

  private final Map<String, AccessRule> rulesForPaths = new HashMap<>();


  public Optional<AccessRule> getAccessRule(Class<?> targetOrLayout)
  {
    return doGetAccessRule(targetOrLayout, true);
  }

  public void setAccessRule(Class<? extends Component> targetOrLayout, AccessRule accessRule)
  {
    rulesForTargetOrLayouts.put(targetOrLayout, accessRule);
  }

  public void removeAccessRule(Class<? extends Component> targetOrLayout)
  {
    rulesForTargetOrLayouts.remove(targetOrLayout);
  }

  protected Optional<AccessRule> doGetAccessRule(Class<?> targetOrLayout,
      boolean includeAnnotations)
  {
    AccessRule accessRule = rulesForTargetOrLayouts.get(targetOrLayout);
    if (accessRule != null)
      return Optional.of(accessRule);

    if (includeAnnotations)
    {
      SecuredAccess securedAccess =
          AnnotationUtils.findAnnotation(targetOrLayout, SecuredAccess.class);
      if (securedAccess != null)
        return Optional.of(AccessRule.asCopyOf(securedAccess));
    }

    return Optional.empty();
  }

  public Map<String, AccessRule> getRegisteredAccessRulesForTargetsAndLayouts()
  {
    return new HashMap<>(rulesForPaths);
  }


  public Optional<AccessRule> getAccessRule(String routePath)
  {
    return Optional.ofNullable(rulesForPaths.get(routePath));
  }

  public void setAccessRule(String routePath, AccessRule accessRule)
  {
    rulesForPaths.put(routePath, accessRule);
  }

  public void removeAccessRule(String routePath)
  {
    rulesForPaths.remove(routePath);
  }

  public Map<String, AccessRule> getRegisteredAccessRulesForPaths()
  {
    return new HashMap<>(rulesForPaths);
  }

}
