package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.server.VaadinSession;
import de.codecamp.vaadin.security.spring.access.AccessRule;
import java.util.Objects;
import java.util.Optional;


public class SessionRouteAccessRuleRegistry
  extends
    AbstractRouteAccessRuleRegistry
{

  private final VaadinSession session;


  public SessionRouteAccessRuleRegistry(VaadinSession session)
  {
    this.session = session;
  }


  public static SessionRouteAccessRuleRegistry getSessionRegistry(VaadinSession session)
  {
    Objects.requireNonNull(session, "session must not be null");

    SessionRouteAccessRuleRegistry registry =
        session.getAttribute(SessionRouteAccessRuleRegistry.class);
    if (registry == null)
    {
      registry = new SessionRouteAccessRuleRegistry(session);
      session.setAttribute(SessionRouteAccessRuleRegistry.class, registry);
    }

    return registry;
  }


  @Override
  public Optional<AccessRule> getAccessRule(String routePath)
  {
    Optional<AccessRule> accessRuleOpt = super.getAccessRule(routePath);
    if (accessRuleOpt.isPresent())
      return accessRuleOpt;

    return getApplicationRegistry().getAccessRule(routePath);
  }

  @Override
  public Optional<AccessRule> getAccessRule(Class<?> targetOrLayout)
  {
    Optional<AccessRule> accessRuleOpt = super.doGetAccessRule(targetOrLayout, false);
    if (accessRuleOpt.isPresent())
      return accessRuleOpt;

    return getApplicationRegistry().getAccessRule(targetOrLayout);
  }


  private AbstractRouteAccessRuleRegistry getApplicationRegistry()
  {
    return ApplicationRouteAccessRuleRegistry
        .getApplicationRegistry(session.getService().getContext());
  }

}
