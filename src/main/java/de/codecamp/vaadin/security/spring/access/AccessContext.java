package de.codecamp.vaadin.security.spring.access;


import de.codecamp.vaadin.security.spring.access.endpoint.EndpointAccessContext;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessContext;
import java.lang.annotation.Annotation;
import java.util.Optional;


/**
 * This class provides the context information to {@link AccessEvaluator AccessEvaluators}, either
 * for a route or an entpoint.
 */
public interface AccessContext
{

  /**
   * The type of the target for which to check access; either a route or and endpoint.
   *
   * @return type of the target for which to check access
   */
  TargetType getTargetType();

  /**
   * A convenience method that allows to find the first matching annotation of a certain type,
   * looking in locations and in an order that makes the most sense for the target type of this
   * access context. This method does support meta-annotations.
   *
   * @param <A>
   *          the annotation type
   * @param annotationType
   *          the annotation type
   * @return the first matching annotation
   */
  <A extends Annotation> Optional<A> findAnnotation(Class<A> annotationType);


  /**
   * Returns whether this is a route access context, i.e. whether {@link #asRouteAccessContext()}
   * can be safely called.
   *
   * @return whether this is a route access context
   */
  default boolean isRoute()
  {
    return (getTargetType() == TargetType.ROUTE);
  }

  /**
   * Casts this access context to a {@link RouteAccessContext} which provides more information that
   * is specific to this target type. Check the {@link #getTargetType() type} before calling this
   * method to see if this is possible.
   *
   * @return this access context as a {@link RouteAccessContext}
   */
  default RouteAccessContext asRouteAccessContext()
  {
    if (getTargetType() != TargetType.ROUTE)
      throw new IllegalStateException("This is not a RouteAccessContext.");

    return (RouteAccessContext) this;
  }

  /**
   * Returns whether this is an endpoint access context, i.e. whether
   * {@link #asEndpointAccessContext()} can be safely called.
   *
   * @return whether this is an endpoint access context
   */
  default boolean isEndpoint()
  {
    return (getTargetType() == TargetType.ENDPOINT);
  }

  /**
   * Casts this access context to a {@link EndpointAccessContext} which provides more information
   * that is specific to this target type. Check the {@link #getTargetType() type} before calling
   * this method to see if this is possible.
   *
   * @return this access context as a {@link EndpointAccessContext}
   */
  default EndpointAccessContext asEndpointAccessContext()
  {
    if (getTargetType() != TargetType.ENDPOINT)
      throw new IllegalStateException("This is not a EndpointAccessContext.");

    return (EndpointAccessContext) this;
  }


  /**
   * The currently supported types of targets for which access can be controlled.
   */
  enum TargetType
  {

    ROUTE,
    ENDPOINT

  }

}
