package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.server.VaadinContext;
import java.util.Objects;


public class ApplicationRouteAccessRuleRegistry
  extends
    AbstractRouteAccessRuleRegistry
{

  public static ApplicationRouteAccessRuleRegistry getApplicationRegistry(VaadinContext context)
  {
    Objects.requireNonNull(context, "context must not be null");

    ApplicationRouteAccessRuleRegistry registry;
    synchronized (context)
    {
      registry = context.getAttribute(ApplicationRouteAccessRuleRegistry.class);
      if (registry == null)
      {
        registry = new ApplicationRouteAccessRuleRegistry();
        context.setAttribute(ApplicationRouteAccessRuleRegistry.class, registry);
      }
    }

    return registry;
  }

}
