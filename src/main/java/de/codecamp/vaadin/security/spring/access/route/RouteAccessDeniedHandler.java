package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.router.AccessDeniedException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.HasErrorParameter;
import java.io.Serializable;
import org.springframework.core.annotation.Order;


/**
 * Handles when a fully authenticated user has been denied access to a route, e.g. by navigating to
 * a different route and/or showing an error message. No further attempt to login the user needs to
 * be made here.
 * <p>
 * Multiple handlers are possible and will be {@link Order ordered} according to Spring's standard
 * mechanism. Once the first handler has requested to reroute or forward to a different view, no
 * further handlers are considered. When no handler reroutes or forwards, a
 * {@link AccessDeniedException} is thrown which could be handled in an {@link HasErrorParameter
 * error view}.
 */
@Order(0) // more useful default than LOWEST_PRECEDENCE
public interface RouteAccessDeniedHandler
  extends
    Serializable
{

  /**
   * Called after a fully authenticated user has been denied access to a route.
   *
   * @param event
   *          navigation event with event details while trying to access the secured route
   */
  void handleAccessDenied(BeforeEnterEvent event);

}
