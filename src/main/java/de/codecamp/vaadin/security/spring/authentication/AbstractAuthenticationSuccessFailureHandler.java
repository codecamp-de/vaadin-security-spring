package de.codecamp.vaadin.security.spring.authentication;


import com.vaadin.flow.server.VaadinSession;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import java.util.Collection;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;


/**
 * This is the base class for implementations of {@link AuthenticationSuccessHandler} and
 * {@link AuthenticationFailureHandler} towards Spring Security.
 */
abstract class AbstractAuthenticationSuccessFailureHandler
{

  private static final Logger LOG = LoggerFactory.getLogger(VaadinAuthenticationService.class);


  protected Optional<VaadinSession> getSession(HttpServletRequest request)
  {
    HttpSession httpSession = request.getSession(false);
    if (httpSession == null)
    {
      LOG.warn("No HttpSession found to store VaadinAuthenticationResult.");
      return Optional.empty();
    }

    /*
     * There could only be more than one VaadinSession if there are multiple Vaadin servlets within
     * the same application. Not sure how to properly handle that so it's not yet supported for now.
     */
    Collection<VaadinSession> sessions = VaadinSession.getAllSessions(httpSession);
    if (sessions.size() > 1)
    {
      throw new IllegalStateException(
          "Multiple VaadinSessions (caused by multiple VaadinServlets) not supported.");
    }

    VaadinSession session = sessions.stream().findFirst().orElse(null);
    if (session == null)
    {
      LOG.warn("No VaadinSession found for HttpServletRequest.");
    }

    return Optional.ofNullable(session);
  }

}
