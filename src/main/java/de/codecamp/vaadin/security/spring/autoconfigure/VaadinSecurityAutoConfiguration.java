package de.codecamp.vaadin.security.spring.autoconfigure;


import com.vaadin.flow.server.auth.AccessAnnotationChecker;
import de.codecamp.vaadin.security.spring.access.DefaultVaadinSecurityExpressionHandler;
import de.codecamp.vaadin.security.spring.access.VaadinSecurityExpressionHandler;
import de.codecamp.vaadin.security.spring.access.endpoint.EndpointAccessChecker;
import de.codecamp.vaadin.security.spring.access.route.DefaultRouteAccessControl;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessControlServiceInitListener;
import de.codecamp.vaadin.security.spring.access.route.RouteAccessDeniedHandler;
import de.codecamp.vaadin.security.spring.authentication.AuthenticationChangeDispatcher;
import de.codecamp.vaadin.security.spring.authentication.AuthenticationResultHandler;
import de.codecamp.vaadin.security.spring.authentication.StandardAuthenticationHandlers;
import de.codecamp.vaadin.security.spring.authentication.StandardAuthenticationService;
import de.codecamp.vaadin.security.spring.authentication.StandardAuthenticationServiceInitListener;
import de.codecamp.vaadin.security.spring.authentication.VaadinSecurityContextHolderStrategy;
import de.codecamp.vaadin.security.spring.authentication.VaadinSecurityContextStorageUIInitListener;
import de.codecamp.vaadin.security.spring.config.VaadinSecurityConfigurer;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.ConditionalOnDefaultWebSecurity;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextHolderStrategy;
import org.springframework.security.web.SecurityFilterChain;


@AutoConfiguration(before = SecurityAutoConfiguration.class)
@EnableConfigurationProperties(VaadinSecurityProperties.class)
public class VaadinSecurityAutoConfiguration
{

  @Bean
  AuthenticationChangeDispatcher vaadinSecurityAuthenticationChangeDispatcher()
  {
    return new AuthenticationChangeDispatcher();
  }

  @Bean
  VaadinSecurityContextStorageUIInitListener vaadinSecurityContextStorageUIInitListener()
  {
    return new VaadinSecurityContextStorageUIInitListener();
  }

  /**
   * Provides the {@link VaadinSecurityContextHolderStrategy} as bean and registers it statically
   * with the {@link SecurityContextHolder}.
   */
  @Bean
  @ConditionalOnMissingBean
  SecurityContextHolderStrategy vaadinSecurityContextHolderStrategy()
  {
    VaadinSecurityContextHolderStrategy bean = new VaadinSecurityContextHolderStrategy();
    SecurityContextHolder.setContextHolderStrategy(bean);
    return bean;
  }


  /*
   * Standard authentication
   */

  @Bean
  @ConditionalOnProperty(
      prefix = "codecamp.vaadin.security.standard-auth",
      name = "enabled",
      matchIfMissing = true)
  StandardAuthenticationService vaadinStandardAuthenticationService(
      List<AuthenticationResultHandler> authenticationResultHandler,
      VaadinSecurityProperties properties)
  {
    StandardAuthenticationService bean = new StandardAuthenticationService();

    URI uiRootUrl = URI.create(properties.getUiRootUrl());
    URI loginProcessingUrl = URI.create(properties.getStandardAuth().getLoginProcessingUrl());
    URI logoutProcessingUrl = URI.create(properties.getStandardAuth().getLogoutProcessingUrl());

    bean.setLoginProcessingClientUrl(uiRootUrl.relativize(loginProcessingUrl).toString());
    bean.setLogoutProcessingClientUrl(uiRootUrl.relativize(logoutProcessingUrl).toString());
    bean.setAuthenticationResultHandlers(authenticationResultHandler);
    return bean;
  }

  @Bean
  @ConditionalOnProperty(
      prefix = "codecamp.vaadin.security.standard-auth",
      name = "enabled",
      matchIfMissing = true)
  StandardAuthenticationHandlers vaadinStandardAuthenticationHandlers(
      VaadinSecurityProperties properties)
  {
    StandardAuthenticationHandlers bean = new StandardAuthenticationHandlers();
    bean.setMainRoute(properties.getStandardAuth().getMainRoute());
    bean.setLoginRoute(properties.getStandardAuth().getLoginRoute());
    return bean;
  }

  @Bean
  @ConditionalOnProperty(
      prefix = "codecamp.vaadin.security.standard-auth",
      name = "enabled",
      matchIfMissing = true)
  StandardAuthenticationServiceInitListener vaadinLoginRouteAccessRuleServiceInitListener(
      VaadinSecurityProperties properties)
  {
    StandardAuthenticationServiceInitListener bean =
        new StandardAuthenticationServiceInitListener();
    bean.setLoginRoute(properties.getStandardAuth().getLoginRoute());
    bean.setMainRoute(properties.getStandardAuth().getMainRoute());
    return bean;
  }



  /*
   * Security expression evaluation
   */

  @Bean
  @ConditionalOnMissingBean
  VaadinSecurityExpressionHandler vaadinSecurityExpressionHandler(
      Optional<RoleHierarchy> roleHierarchy)
  {
    DefaultVaadinSecurityExpressionHandler bean = new DefaultVaadinSecurityExpressionHandler();
    roleHierarchy.ifPresent(bean::setRoleHierarchy);
    return bean;
  }



  /*
   * Route access control
   */

  @Bean
  DefaultRouteAccessControl vaadinRouteAccessControl(
      List<RouteAccessDeniedHandler> vaadinRouteAccessDeniedHandlers,
      VaadinSecurityProperties properties)
  {
    DefaultRouteAccessControl bean = new DefaultRouteAccessControl();
    bean.setDenyUnsecured(properties.getDenyUnsecured());
    bean.setAccessDeniedHandlers(vaadinRouteAccessDeniedHandlers);
    return bean;
  }

  @Bean
  RouteAccessControlServiceInitListener vaadinRouteAccessControlServiceInitListener()
  {
    return new RouteAccessControlServiceInitListener();
  }



  /*
   * Endpoint access control
   */


  @Configuration(proxyBeanMethods = false)
  @ConditionalOnClass(dev.hilla.auth.EndpointAccessChecker.class)
  static class EndpointSecurityConfiguration
  {

    @Bean
    @Primary
    @ConditionalOnBean({AccessAnnotationChecker.class})
    dev.hilla.auth.EndpointAccessChecker vaadinSecurityEndpointAccessChecker(
        AccessAnnotationChecker accessAnnotationChecker)
    {
      return new EndpointAccessChecker(accessAnnotationChecker);
    }

  }



  /*
   * Default web security configuration for Vaadin
   */


  /**
   * Overrides the default security configuration provided by Spring. That's why this
   * auto-configuration must be run before {@link SecurityAutoConfiguration}.
   */
  @Bean
  @ConditionalOnDefaultWebSecurity
  @Order(SecurityProperties.BASIC_AUTH_ORDER)
  SecurityFilterChain defaultVaadinSecurityFilterChain(HttpSecurity http)
    throws Exception
  {
    http.apply(new VaadinSecurityConfigurer());
    return http.build();
  }

}
