package de.codecamp.vaadin.security.spring.access;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.di.Instantiator;
import org.springframework.context.ApplicationContext;


/**
 * An {@link AccessEvaluator} is the programmatic, more flexible alternative to expression-based
 * access control for routes and endpoints.
 * <p>
 * Depending on the context, access evaluators are instantiated in different ways:
 * <ul>
 * <li>Where a {@link UI} is available (which is the case for routes),
 * {@link Instantiator#getOrCreate(Class)} will be used. I.e. the evaluator either needs to be
 * available as a Spring bean or it requires a public no-args constructor.</li>
 * <li>Otherwise (for endpoints) the evaluator is directly requested from the
 * {@link ApplicationContext} and therefore needs to be available as a Spring bean.</li>
 * </ul>
 */
public interface AccessEvaluator
{

  /**
   * Returns whether access should be granted based on the given context.
   * <p>
   * {@link VaadinSecurity} can be used in implementations.
   *
   * @param accessContext
   *          the access context
   * @return whether access should be granted
   */
  boolean hasAccess(AccessContext accessContext);

}
