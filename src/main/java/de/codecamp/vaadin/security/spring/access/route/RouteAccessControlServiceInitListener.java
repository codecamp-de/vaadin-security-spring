package de.codecamp.vaadin.security.spring.access.route;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.di.Instantiator;
import com.vaadin.flow.router.AccessDeniedException;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterListener;
import com.vaadin.flow.router.ListenerPriority;
import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.UIInitEvent;
import com.vaadin.flow.server.UIInitListener;
import com.vaadin.flow.server.VaadinServiceInitListener;
import com.vaadin.flow.server.startup.ApplicationRouteRegistry;
import java.util.Collections;


/**
 * Registers the {@link RouteAccessControl} with every new {@link UI}.
 */
public class RouteAccessControlServiceInitListener
  implements
    VaadinServiceInitListener,
    UIInitListener
{

  private static final VaadinAccessControlAdapter ACCESS_CONTROL_ADAPTER =
      new VaadinAccessControlAdapter();


  @Override
  public void serviceInit(ServiceInitEvent serviceInitEvent)
  {
    /*
     * This code is reached after registering all HasErrorParameters the usual way. So only register
     * the default error view for RouteAccessDeniedExceptions when no other is found.
     */
    ApplicationRouteRegistry routeRegistry =
        ApplicationRouteRegistry.getInstance(serviceInitEvent.getSource().getContext());
    if (routeRegistry.getConfiguration()
        .getExceptionHandlerByClass(AccessDeniedException.class) == null)
    {
      routeRegistry
          .setErrorNavigationTargets(Collections.singleton(DefaultRouteAccessDeniedError.class));
    }
  }

  @Override
  public void uiInit(UIInitEvent event)
  {
    // register the actual access control
    event.getUI().addBeforeEnterListener(ACCESS_CONTROL_ADAPTER);
  }


  @ListenerPriority(RouteAccessControl.PRIORITY)
  private static class VaadinAccessControlAdapter
    implements
      BeforeEnterListener
  {

    @Override
    public void beforeEnter(BeforeEnterEvent event)
    {
      Instantiator.get(event.getUI()).getOrCreate(RouteAccessControl.class).checkAccess(event);
    }

  }

}
