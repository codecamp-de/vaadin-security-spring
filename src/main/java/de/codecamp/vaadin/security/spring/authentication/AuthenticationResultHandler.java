package de.codecamp.vaadin.security.spring.authentication;


import java.io.Serializable;
import org.springframework.core.annotation.Order;


/**
 * A handler that will be notified with the result once a login attempt has completed. Only called
 * as part of standard authentication.
 */
@Order(0) // more useful default than LOWEST_PRECEDENCE
@FunctionalInterface
public interface AuthenticationResultHandler
  extends
    Serializable
{

  /**
   * Called with the result of the authentication attempt.
   *
   * @param result
   *          the authentication result
   * @return whether the result should be considered handled and no further handler should be
   *         consulted; this can also prevent the default navigation after successfully logging in
   *         when on the login view
   */
  boolean handleAuthenticationResult(AuthenticationResult result);

}
